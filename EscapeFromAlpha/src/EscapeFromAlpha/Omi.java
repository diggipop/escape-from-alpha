package EscapeFromAlpha;

import java.util.ArrayList;

public class Omi extends BaseItem {
	// Beispiel zum Erstellen eines Omi anhand einer T�r:
	//
	// Omi lookedDoor = new Omi("Verschlossene T�r",
	//							"TEXT BEI BETRACHTEN",
	//							"TEXT BEI BENUTZEN",
	//							false,	<-- wird nicht beim benutzen gel�scht
	//							null);  <-- keine verbindung zu einem Raum
	//
	// Omi lookedDoor = new Omi("offene T�r",
	//							"TEXT BEI BETRACHTEN",
	//							"TEXT BEI BENUTZEN",
	//							false, 				<-- wird nicht beim benutzen gel�scht
	//							einAndererRaum);	<-- verbindet zu Raum "einAndererRaum", dh einAndererRaum taucht in der GeheZu-Liste auf
	//
	//
	// Wenn lookedDoor mit theKeyItem kombiniert wird, wird lookedDoor entfernt und durch unlookedDoor ersetzt.
	// 		lookedDoor.addCombination(theKeyItem,unlookedDoor,true);
	//
	// Was mit theKeyItem passieren soll, muss am Item definiert werden. (Es soll entfernt werden und kein neues Item erzeugen)
	// 		theKeyItem.additemtocombinate(lookedDoor, null, true);

	Boolean removeOnUse = false;
	Boolean showForUse = false;
	
	// Liste mit Items die dieses Object enth�lt
	ArrayList<Item> containedItems = new ArrayList<Item>();
	
	Room connectRoomTo = null;
	
	// Liste mit Kombinationsm�glichkeiten und deren Resultaten
	// TODO: evtl. noch eine Liste mit Texten (nur wenn n�tig)
	// Anmerkung:
	//    Omi kann nur zu einem anderem Omi werden wenn es mit etwas kombiniert wird.
	//    Soll aber ein Item entstehen, so ist die Kombinationsm�glichkeit am Item zu definieren.

	Omi(String name, String textOnObserve,String textOnUse, Boolean removeOnUse) {
		super(name,textOnObserve,textOnUse);
	}
}
