package EscapeFromAlpha;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PanelTakeList extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Image backgroundImage;
	private DefaultListModel<CustomListEntry> data = new DefaultListModel<CustomListEntry>();
	private JList<CustomListEntry> list;

	public void update() {
		backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
		data.removeAllElements();

		for (Omi omi : GameLogic.currentGame.currentRoom.containedOmis) {
			if (omi.observed) {
				for (BaseItem item : omi.containedItems) {
					data.addElement(new CustomListEntry(item.name, false, item.observed));
				}
			}
		}
	}

	public PanelTakeList() {
		setBackground(Color.GREEN);
		setBorder(new EmptyBorder(50, 150, 50, 150));
		setLayout(new BorderLayout(0, 0));

		list = new JList<CustomListEntry>(data);
		list.setFont(new Font("Arial", Font.PLAIN, 16));
		list.setForeground(Color.WHITE);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (list.getSelectedIndex() >= 0) {
					GameLogic.currentGame.takeListSelectionChanged(((CustomListEntry) list.getSelectedValue()).text);
				}
			}
		});
		list.setBackground(new Color(0, 0, 0, .5f));
		list.setCellRenderer(new CustomCellRenderer());
		add(list, BorderLayout.CENTER);

		JTextPane header = new JTextPane();
		header.setForeground(Color.WHITE);
		header.setFont(new Font("Arial", Font.PLAIN, 22));
		header.setBackground(new Color(0, 0, 0, .5f));
		header.setText("Was m�chtest du nehmen?");
		header.setEditable(false);
		add(header, BorderLayout.NORTH);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.paintComponents(g);
	}
}
