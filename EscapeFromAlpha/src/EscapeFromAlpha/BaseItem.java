package EscapeFromAlpha;

import java.util.ArrayList;

public class BaseItem {
	String name = "noName";
	String textOnObserve = "";
	String textOnUse = "";
	Boolean observed = false;
	
	ArrayList<BaseItem> itemstocombinate = new ArrayList<BaseItem>();
	ArrayList<BaseItem> itemscombinated = new ArrayList<BaseItem>();
	ArrayList<Boolean> itemdestruoncombinate = new ArrayList<Boolean>();
	ArrayList<String> textOnCombinate = new ArrayList<String>();
	ArrayList<Room> connectToRoomOnCombinate = new ArrayList<Room>();
	ArrayList<Runnable> execOnCombinate = new ArrayList<Runnable>();
	
	BaseItem(String name, String textOnObserve,String textOnUse) {
		this.name = name;
		this.textOnObserve = textOnObserve;
		this.textOnUse = textOnUse;
	}
	
	public void addItemCombination(String textOnCombinate, BaseItem otherItem, BaseItem combinateTo, boolean removeOnCombinate, Room connectToOnCombinate, Runnable code) {
		this.textOnCombinate.add(textOnCombinate);
		itemstocombinate.add(otherItem);
		itemscombinated.add(combinateTo);
		itemdestruoncombinate.add(removeOnCombinate);
		connectToRoomOnCombinate.add(connectToOnCombinate);
		execOnCombinate.add(code);
	}		
	
	public void addItemCombination(String textOnCombinate, BaseItem otherItem, BaseItem combinateTo, boolean removeOnCombinate, Room connectToOnCombinate) {
		this.textOnCombinate.add(textOnCombinate);
		itemstocombinate.add(otherItem);
		itemscombinated.add(combinateTo);
		itemdestruoncombinate.add(removeOnCombinate);
		connectToRoomOnCombinate.add(connectToOnCombinate);
		execOnCombinate.add(null);
	}	
	
	public void addItemCombination(String textOnCombinate, BaseItem otherItem, BaseItem combinateTo, boolean removeOnCombinate) {
		this.textOnCombinate.add(textOnCombinate);
		itemstocombinate.add(otherItem);
		itemscombinated.add(combinateTo);
		itemdestruoncombinate.add(removeOnCombinate);
		connectToRoomOnCombinate.add(null);
		execOnCombinate.add(null);
	}
	public void addItemCombination(BaseItem otherItem, BaseItem combinateTo, boolean removeOnCombinate) {
		this.textOnCombinate.add(null);
		itemstocombinate.add(otherItem);
		itemscombinated.add(combinateTo);
		itemdestruoncombinate.add(removeOnCombinate);
		connectToRoomOnCombinate.add(null);
		execOnCombinate.add(null);
	}	
}
