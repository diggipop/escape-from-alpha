package EscapeFromAlpha;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.FactoryRegistry;
import javazoom.jl.player.advanced.AdvancedPlayer;
import java.io.File;
// extend
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.*;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class MusikPlayer {

	private AdvancedPlayer player;

	// extend

	private InputStream is;

	private Player _player;
	private boolean repeat;
	private boolean paused;
	private long pauseLocation;
	private long totalSongLength;
	private String musicFilePath;

	public MusikPlayer() {
		player = null;
	}

	public void dateiAnspielen(String dateiname) {

		File f = new File(dateiname);

		if (f.exists() && !f.isDirectory()) {
			System.out.println("vorhanden");
		} else {
			System.out.println("nicht vorhanden");
		}

		try {
			playerVorbereiten(dateiname);
			player.play(500);
		} catch (JavaLayerException e) {
			meldeProblem(dateiname);
		} finally {
			killPlayer();
		}
	}

	public void starteAbspielen(final String dateiname) {
		try {
			playerVorbereiten(dateiname);
			Thread playerThread = new Thread() {
				public void run() {
					try {
						player.play(5000);
					} catch (JavaLayerException e) {
						meldeProblem(dateiname);
					} finally {
						killPlayer();
					}
				}
			};
			playerThread.start();
		} catch (Exception ex) {
			meldeProblem(dateiname);
		}
	}

	public void stop() {
		killPlayer();
	}

	private void playerVorbereiten(String dateiname) {
		try {
			InputStream is = gibEingabestream(dateiname);
			player = new AdvancedPlayer(is, erzeugeAudiogeraet());
		} catch (IOException e) {
			meldeProblem(dateiname);
			killPlayer();
		} catch (JavaLayerException e) {
			meldeProblem(dateiname);
			killPlayer();
		}
	}

	private InputStream gibEingabestream(String dateiname) throws IOException {
		return new BufferedInputStream(getClass().getResourceAsStream(dateiname));
	}

	private AudioDevice erzeugeAudiogeraet() throws JavaLayerException {
		return FactoryRegistry.systemRegistry().createAudioDevice();
	}

	private void killPlayer() {
		synchronized (this) {
			if (player != null) {
				player.close();
				player = null;
			}
		}
	}

	private void meldeProblem(String dateiname) {
		System.out.println("Es gab ein Problem beim Abspielen von: " + dateiname);
	}

	// extend

	public void play(String musicFilePath)
			throws FileNotFoundException, JavaLayerException, IOException, URISyntaxException {

		this.musicFilePath = musicFilePath;

		is = this.getClass().getResourceAsStream(musicFilePath);

		totalSongLength = is.available();

		_player = new Player(is);

		new Thread() {
			@Override
			public void run() {
					try {
						_player.play();
						if (_player.isComplete() && repeat) {
							play(musicFilePath);
						}

					} catch (JavaLayerException | IOException ex) {
						System.err.println("::: there was an error to play " + musicFilePath);
					} catch (URISyntaxException ex) {
						System.err.println("::: there was an error to play " + ex);
						// Logger.getLogger(MusicPlayer.class.getName()).log(Level.SEVERE, null, ex);
					}

			}////
		}.start();///

	}//

	/**
	 * use this method to remuse current paused song
	 * 
	 * @throws FileNotFoundException
	 * @throws JavaLayerException
	 * @throws IOException
	 * @throws java.net.URISyntaxException
	 */
	public void resume() throws FileNotFoundException, JavaLayerException, IOException, URISyntaxException {

		paused = false;

		is = this.getClass().getResourceAsStream(musicFilePath);

		is.skip(totalSongLength - pauseLocation);

		_player = new Player(is);

		new Thread() {
			@Override
			public void run() {
				try {
					_player.play();
				} catch (JavaLayerException ex) {
					System.err.println("::: there was an error to play " + musicFilePath);
				}
			}////
		}.start();///

	}//

	/**
	 * use this method to stop current song that is being played
	 */
	public void _stop() {
		paused = false;

		if (null != _player) {
			_player.close();

			totalSongLength = 0;
			pauseLocation = 0;
		} ///

	}//

	/**
	 * use this method to pause current played song
	 */
	public void pause() {

		paused = true;
		if (null != _player) {
			try {
				pauseLocation = is.available();
				_player.close();
			} catch (IOException ex) {
				System.out.println("::: error when song is paused");
			}

		} ///

	}//

	/**
	 * 
	 * @return true if the song i will start once is done, false if not
	 */
	public boolean isRepeat() {
		return repeat;
	}

	/**
	 * set if the song will start once is done
	 * 
	 * @param repeat
	 */
	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
	}

	public boolean isPaused() {
		return paused;
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}
}