package EscapeFromAlpha;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import java.awt.Color;
import javax.swing.BoxLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JLayeredPane;
import java.awt.Component;

public class PanelRoom extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Image backgroundImage;
	private JLabel timerLabel;
	private JTextPane textPane;
	private JTextPane inventarPane;
	private JLayeredPane layeredPane;

	public void update() {
		if (GameLogic.currentGame.currentRoom != null)
			backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
	}

	public void setText(String text) {
		this.textPane.setText(text);
	}

	public String CalulateTime(int _time) {
		String _timeString = "";

		int h, m, s, _R;
		h = _time / 3600;
		_R = _time - h * 3600;
		m = _R / 60;
		_R = _R - m * 60;
		s = _R;

		_timeString += AddToSting(h,true);
		_timeString +=AddToSting(m,true);
		_timeString +=AddToSting(s,false);
		
		return _timeString;
	}

	public String AddToSting(int _t, boolean _b) {
		String string = "";
		if (_t < 10) {
			string = "0" + _t;
		} else {
			string = _t + "";
		}
		// zeittrenner
		string += _b ? ":" : "" ;
		return string;
	}
	
	
	public void setDisplayedTime(int time) {
		timerLabel.setText("Sauerstoff reicht noch f�r " + CalulateTime(time));
	}

	public void setDisplayedInventoryText(String text) {
		inventarPane.setText("Inventar" + text);
	}

	public PanelRoom() {
		setBackground(Color.YELLOW);
		setBorder(new EmptyBorder(50, 50, 50, 50));
		setLayout(new BorderLayout(0, 0));

		textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setFont(new Font("Arial", Font.PLAIN, 22));
		textPane.setForeground(Color.WHITE);
		textPane.setBackground(new Color(0, 0, 0, 0.5f));

		StyleContext.NamedStyle centerStyle = StyleContext.getDefaultStyleContext().new NamedStyle();
		StyleConstants.setAlignment(centerStyle, StyleConstants.ALIGN_CENTER);
		centerStyle.addAttribute(StyleConstants.Foreground, Color.white);
		centerStyle.addAttribute(StyleConstants.FontSize, 18);
		centerStyle.addAttribute(StyleConstants.FontFamily, "arial");
		centerStyle.addAttribute(StyleConstants.Bold, false);
		textPane.setLogicalStyle(centerStyle);

		add(textPane, BorderLayout.CENTER);

		inventarPane = new JTextPane();
		inventarPane.setEditable(false);
		inventarPane.setFont(new Font("Arial", Font.PLAIN, 18));
		inventarPane.setForeground(new Color(0,166,255,255));
		inventarPane.setBackground(new Color(0, 0, 0, 0.5f));
		inventarPane.setText("Inventar");
		add(inventarPane, BorderLayout.SOUTH);

		layeredPane = new JLayeredPane();
		layeredPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		layeredPane.setOpaque(true);
		layeredPane.setBackground(new Color(0, 0, 0, 0.5f));
		layeredPane.setLayout(new BoxLayout(layeredPane, BoxLayout.Y_AXIS));
		add(layeredPane, BorderLayout.NORTH);

		timerLabel = new JLabel("Sauerstoff reicht noch f�r 01:30:00");
		timerLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		timerLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		timerLabel.setForeground(Color.RED);
		timerLabel.setFont(new Font("Arial", Font.BOLD, 18));
		layeredPane.add(timerLabel);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.paintComponents(g);
	}
}
