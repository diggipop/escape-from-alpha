package EscapeFromAlpha;
/**
 * 
 * Klasse zum abspielen von mp3 tracks
 *
 */
public class Music extends MusikPlayer {
	
    private boolean _isPlaying = false;
    
    private final String PATH = "";

    /**
     * sollte automatisch den alten track stoppen bevor der neue anf�ngt
     * @param _trackName
     */
    public void playing(String _trackName) {
    	
    	if (_isPlaying) {
    		stop();
    	}
    	starteAbspielen(PATH + _trackName);
    	
    	_isPlaying = true;
    }
    
	public Music()
    {
    	super();
    }
    
}
