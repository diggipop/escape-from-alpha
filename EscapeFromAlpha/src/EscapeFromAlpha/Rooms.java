package EscapeFromAlpha;

import java.util.ArrayList;

public class Rooms {
	public static ArrayList<Room> rooms = new ArrayList<Room>();

	public static Room getRoomByName(String name) {
		for (Room room : rooms) {
			if (room.name.equals(name)) {
				return room;
			}
		}
		System.out.println("ERROR: getRoomByName could not find " + name);
		return null;
	}

	Rooms() {
		Room garden = new Room("Garten", "Der hydroponische Garten der Station erstrahlt in hellem weißen Licht. Dieser Bereich der Station scheint trotz Notlage stets gut beleuchtet zu sein. Die Wände des Raumes sind strahlend weiß und bilden einen angenehmen Kontrast zu dem Grün der Pflanzen. Hier befinden sich diverse große, flache Metalltische, auf denen Pflanzen heranwachsen. Es gibt auch eine Art Wiese mit Büschen und Bäumen. Du zweifelst an ihrer Echtheit.", "Garten.png");
		garden.addOmi(Omis.omis.get(8));
		garden.addOmi(Omis.omis.get(0));
		garden.addOmi(Omis.omis.get(35));
		garden.addOmi(Omis.getOmiByName("Garten-Boden"));

		rooms.add(garden);

		Room storage = new Room("Lagerraum", "Der schwach beleuchtete Raum sieht aus wie jedes andere Lager. "
				+ "Die Wände stehen voll mit gut gepackten Lagerregalen. In einer Ladenische steht ein kleiner entladener Gabelstapler. "
				+ "In einer anderen Ecke leuchten zwei rote Punkte zu dir herüber.", "Lager.png");
		storage.addOmi(Omis.omis.get(33));
		storage.addOmi(Omis.omis.get(14));
		storage.addOmi(Omis.omis.get(17));
		storage.addOmi(Omis.omis.get(1));

		rooms.add(storage);

		Room floor2 = new Room("Area B",
				"Der bisher unzugängliche Teil des Flures liegt im gleichen Dämmerlicht wie die restliche Station. Es lassen sich die Türschilder für den Maschinenraum, das Lager, Area A sowie das verheißungsvolle Schild “Schleuse“. Neben der Schleusentür erkennst du das Tür-Panel unter welchem eine schlimm- zugerichtete Astronautin in einem völlig zerfetzten Raumanzug an der Wand lehnt. Scheinbar hat sie ihre letzte Kraft dafür aufgewendet, die Tür zu verriegeln.",
				"Flur02.png");

		floor2.addOmi(Omis.omis.get(32));
		floor2.addOmi(Omis.omis.get(34));

		rooms.add(floor2);

		Room quarters = new Room("Quartiere",
				"Die Notbeleuchtung vermittelt ein in rot getauchtes Bild deiner Umgebung. Im Raum stehen einzelne spartanische Betten. "
						+ "Eine Reihe Spinte stehen an einer Wand. An den Spinten sind Namen angebracht, die dir nur wage bekannt vorkommen. "
						+ "In einem der Betten liegen die Überreste eines Menschen. Der Boden ist irgendwie klebrig.",
				"Quartier.png");
		quarters.addOmi(Omis.omis.get(25));
		quarters.addOmi(Omis.omis.get(2));
		quarters.addOmi(Omis.omis.get(30));

		rooms.add(quarters);

		Room wc = new Room("WC",
				"Ein kleiner düsterer Raum. Das elektrische Licht an der Decke flackert und lässt dich den Raum nur schemenhaft erkennen. "
						+ "Er wirkt verdreckt und heruntergekommen. Im Raum befinden sich zwei separate Kabinen, "
						+ "eine steht offen, die Tür der anderen ist angelehnt. In der offenen Kabine ist eine Absaugeinrichtung für Toilettengänge erkennbar."
						+ " An der Wand außerhalb der Kabinen befindet sich ein großer blut-beschmierter Spiegel.",
				"Wc.png", "Track_2.mp3");

		wc.addOmi(Omis.omis.get(4));
		wc.addOmi(Omis.omis.get(9));
		wc.addOmi(Omis.omis.get(16));
		wc.addOmi(Omis.omis.get(26));
		rooms.add(wc);

		Room floor1 = new Room("Flur",
				"Ein langer dämmeriger Flur. Die Wände wirken fleckig, an einigen Stellen kannst du Beschädigungen erkennen. "
						+ "Durch das Halbdunkel leuchten die Türschilder über den verschiedenen Türen, die von dem Flur abzweigen. "
						+ "Du erkennst acht Schilder: Quartiere, WC, Erholung, Kantine, Fitnessraum, Gärten, Brücke und Area B."
						+ "Sowie ein riesiges schwarzes Alien.",
				"Flur.png");
		floor1.addOmi(Omis.omis.get(32));
		floor1.addOmi(Omis.omis.get(11));
		floor1.addOmi(Omis.omis.get(18));
		floor1.addOmi(Omis.getOmiByName("Flur-Boden"));
		rooms.add(floor1);

		Room recreationroom = new Room("Erholungsraum",
				"Ein mittelgroßer Raum für Erholungszwecke. Es herrscht ein großes Durcheinander: all-mögliche Einrichtungsgegenstände liegen im Raum verteilt, aus Tischen und Stühlen wurden scheinbar Barrikaden errichtet. Nur in einer hinteren Ecke zeichnen sich in den Schatten ein paar intakte Stühle an einem Tisch sowie ein Billardtisch ab.",
				"Spa.png");
		recreationroom.addOmi(Omis.omis.get(3));
		recreationroom.addOmi(Omis.omis.get(29));
		recreationroom.addOmi(Omis.omis.get(23));
		recreationroom.addOmi(Omis.omis.get(24));
		rooms.add(recreationroom);

		Room canteen = new Room("Kantine",
				"Die Kantine ist ein großer Raum mit Esstischen, einer größeren Küchenzeile und einer Bar. Mag sie einst auch weiß geglänzt haben, so zeigt sich nun auch hier ein Bild der Verwüstung. Der große Kühlschrank steht leicht offen und ein Spalt Licht aus seinem Inneren zeichnet sich auf Wand und Boden ab.",
				"Kantine.png");
		canteen.addOmi(Omis.omis.get(13));
		canteen.addOmi(Omis.omis.get(12));
		canteen.addOmi(Omis.omis.get(28));
		rooms.add(canteen);

		Room gym = new Room("Fitnessraum",
				"Dieser Raum scheint vom allgemeinen Chaos nahezu unberührt geblieben zu sein. "
						+ "Trotz roter Notbeleuchtung erkennst du einen typischen Sportraum mit Hantelbänken, Laufbändern, "
						+ "einem kleinen Boxring und mit einem großen Sandsack. Der Sandsack wirkt irgendwie seltsam deformiert.",
				"Fitnessraum.png");
		gym.addOmi(Omis.omis.get(19));
		gym.addOmi(Omis.omis.get(10));
		gym.addOmi(Omis.omis.get(5));
		rooms.add(gym);

		Room machineroomdark = new Room("Maschinenraum ",
				"Die Notbeleuchtung funktioniert hier nicht. Der Raum ist tiefschwarz. "
						+ "Außer dem Leuchten von einigen Armaturen in den Tiefen des Raumes und dem gelegentlichen Blitzen von "
						+ "freigelegten beschädigten Kabeln kannst du absolut nichts erkennen.",
				"Flur02.png");
		rooms.add(machineroomdark);

		Room machineroom = new Room("Maschinenraum",
				"Der riesige Raum ist das Herz der Station. Im Pegel der Taschenlampe lassen sich gewaltige, beschädigte Sauerstofftanks "
						+ "und die Bedienungseinheit der dazugehörigen Luftversorgungsapparatur erkennen. Außerdem schält sich der einst hochmoderne "
						+ "Generator aus der Dunkelheit. Einige riesige Kisten liegen wie Holzklötzchen im Raum verteilt. Scheinbar beinhalten sie Ersatzeile.",
				"Maschinenraum.png");
		machineroom.addOmi(Omis.omis.get(22));
		machineroom.addOmi(Omis.omis.get(7));
		machineroom.addOmi(Omis.omis.get(20));
		machineroom.addOmi(Omis.getOmiByName("Maschinenraum-Boden"));
		rooms.add(machineroom);

		Room lock = new Room("Schleuse",
				"Der Übergangsraum, der zur Mondoberfläche führt. Das schwache Licht erleuchtet einen scheinbar komplett leeren Raum. "
						+ "Alle Raumanzüge und Sauerstoffflaschen sind aus den Halterungen verschwunden. In einer Ecke liegt ein leerer Werkzeugkoffer. "
						+ "Neben der Tür zum Mond hängt eine ausgeleuchtete Hinweistafel, die einen Blick wert sein könnte.",
				"Luftschleuse.png");
		lock.addOmi(Omis.omis.get(21));
		lock.addOmi(Omis.omis.get(37));
		lock.addOmi(Omis.omis.get(40));

		rooms.add(lock);

		Room commandcentral = new Room("Kommandozentrale",
				"Das geistige Zentrum der Raumstation ist weitestgehend intakt, allerdings weisen die meisten Steuerkonsolen tiefe Furchen auf. Die Wand neben dem Zugang ist mit kleinen schwarzen Löchern übersät, der Kampf um die Station scheint hier am heftigsten getobt zu haben. Etwas tropft auf deine Schulter. Als du nach oben schaust, erkennst du die, an die Decke gepinnte, Leiche eines Menschen. Auf der blutigen Uniform kannst du “Commander Sisko“ lesen.",
				"Flur.png");
		commandcentral.addOmi(Omis.omis.get(36));
		commandcentral.addOmi(Omis.omis.get(27));
		commandcentral.addOmi(Omis.omis.get(6));
		rooms.add(commandcentral);

		// Raum-Verbindungen
		wc.connections.add(Rooms.getRoomByName("Flur"));
		quarters.connections.add(Rooms.getRoomByName("Flur"));

		canteen.connections.add(Rooms.getRoomByName("Flur"));
		canteen.connections.add(Rooms.getRoomByName("Erholungsraum"));
		canteen.connections.add(Rooms.getRoomByName("Fitnessraum"));

		gym.connections.add(Rooms.getRoomByName("Flur"));
		gym.connections.add(Rooms.getRoomByName("Kantine"));

		recreationroom.connections.add(Rooms.getRoomByName("Flur"));
		recreationroom.connections.add(Rooms.getRoomByName("Kantine"));

		garden.connections.add(Rooms.getRoomByName("Flur"));

		storage.connections.add(Rooms.getRoomByName("Garten"));

		floor1.connections.add(Rooms.getRoomByName("WC"));
		floor1.connections.add(Rooms.getRoomByName("Quartiere"));
		floor1.connections.add(Rooms.getRoomByName("Kantine"));
		floor1.connections.add(Rooms.getRoomByName("Garten"));
		floor1.connections.add(Rooms.getRoomByName("Fitnessraum"));
		floor1.connections.add(Rooms.getRoomByName("Erholungsraum"));

		floor2.connections.add(Rooms.getRoomByName("Maschinenraum "));
		floor2.connections.add(Rooms.getRoomByName("Lagerraum"));

		machineroom.connections.add(Rooms.getRoomByName("Area B"));
		machineroomdark.connections.add(Rooms.getRoomByName("Area B"));

		lock.connections.add(Rooms.getRoomByName("Area B"));

		commandcentral.connections.add(Rooms.getRoomByName("Flur"));
	}
}