/**
 * Check the Password Input 
 */
package EscapeFromAlpha;

import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;

public class CheckPassword extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	// Secret
	private static char[] correctPassword = { 'L', 'U', 'N', 'A' };

	// Command String
	private static String OK = "Enter";
	private static String ENTER_PW = "Input";

	// Font
	private static String FONT = "Arial";
	private static int FONT_SIZE = 20;
	private static int FONT_SIZE2 = 15;
	private static int FONT_STYLE = 1;
	private JPasswordField passwordField;

	private Image backgroundImage;
	private Room passwordRoom = Rooms.getRoomByName("Area B");

	// constructor
	public CheckPassword() {
		// controllingFrame = f;
		// Create everything.
		JTextPane textPane = new JTextPane();
		setBackground(Color.YELLOW);
		setBorder(new EmptyBorder(100, 100, 50, 100));

		backgroundImage = passwordRoom.backgroundImage;
		
		// raum text
		textPane.setLayout(new BorderLayout(0, 0));

		JComponent buttonPane = createButtonPanel();
		setLayout(new BorderLayout(0, 0));
		textPane.setFont(new Font("Arial", Font.PLAIN, 22));
		textPane.setForeground(Color.WHITE);
		textPane.setBackground(new Color(0, 0, 0, 0.5f));

		add(textPane);
		add(buttonPane);

		StyleContext.NamedStyle centerStyle = StyleContext.getDefaultStyleContext().new NamedStyle();
		StyleConstants.setAlignment(centerStyle, StyleConstants.ALIGN_CENTER);
		centerStyle.addAttribute(StyleConstants.Foreground, Color.white);
		centerStyle.addAttribute(StyleConstants.FontSize, 18);
		centerStyle.addAttribute(StyleConstants.FontFamily, "arial");
		centerStyle.addAttribute(StyleConstants.Bold, false);

		add(textPane, BorderLayout.CENTER);

		// textPane.setLayout(new BorderLayout(0, 0));

		JTextArea text = new JTextArea("Die T�r zur Schleuse leuchtet rot. Neben der T�r befindet sich ein Display, in das du ein vierstelliges Passwort eingeben musst. Bitte gib jetzt das Passwort ein.", 5, 1);

		text.setEditable(false);
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		text.setOpaque(false);
		// text.setHorizontalAlignment(SwingConstants.CENTER);
		textPane.add(text, BorderLayout.NORTH);
		// text.setHorizontalAlignment(SwingConstants.CENTER);
		text.setBackground(new Color(0,0,0,0.8f));
		text.setForeground(Color.white);
		text.setFont(new java.awt.Font(FONT, FONT_STYLE, FONT_SIZE2));

		JLabel label = new JLabel("Passwort mit Enter best�tigen : ");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.white);
		label.setFont(new java.awt.Font(FONT, FONT_STYLE, FONT_SIZE));

		textPane.add(label, BorderLayout.WEST);
		label.setLabelFor(passwordField);

		passwordField = new JPasswordField(3);
		textPane.add(passwordField, BorderLayout.SOUTH);
		passwordField.setEchoChar('?');
		passwordField.setFont(new java.awt.Font(FONT, FONT_STYLE, FONT_SIZE));
		passwordField.setActionCommand(OK);
		passwordField.addActionListener(this);
		passwordField.setSize(0, 10);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();

		// Debugging
		System.out.println("input " + getString(passwordField.getPassword()));

		if (OK.equals(cmd)) {
			// Process the password.
			char[] input = passwordField.getPassword();
			if (isInputCorrect(input)) {
				System.out.println("Passwort OK");
				
				GameLogic.currentGame.currentRoom.containedOmis.remove(Omis.getOmiByName("Verschlossene T�r zur Schleuse"));
				GameLogic.currentGame.currentRoom.connections.add(Rooms.getRoomByName("Schleuse"));
				GameWindow.panelRoom.setText("Du hast das richtige Passwort eingegeben. Die Codekonsole gibt ein irgendwie zufrieden klingendes Piepen von sich, die T�r �ffnet sich zischend und gibt den Blick auf die Schleuse frei.");
				GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			} else {
				System.out.println("Passwort FALSCH");
				
				GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
				GameWindow.panelRoom.setText("Dieses Passwort ist nicht korrekt.");				
			}

			passwordField.selectAll();
			resetFocus();
		}
	}

	protected JComponent createButtonPanel() {
		// init Grid Layout
		JPanel p = new JPanel(new GridLayout(0, 2));

		JButton okButton = new JButton(ENTER_PW);
		okButton.setFont(new java.awt.Font(FONT, FONT_STYLE, FONT_SIZE));

		okButton.setActionCommand(OK);
		okButton.addActionListener(this);

		p.add(okButton);

		return p;
	}

	private static boolean isInputCorrect(char[] input) {
		boolean isCorrect = true;

		if (input.length != correctPassword.length) {
			isCorrect = false;
		} else {
			for (int i = 0; i < input.length; i++) {
				if (Character.toUpperCase(input[i]) != correctPassword[i]) {
					isCorrect = false;
				}
			}
		}

		return isCorrect;
	}

	/**
	 * for Debugging
	 * 
	 * @param input
	 * @return String
	 */
	protected String getString(char[] input) {

		String _input = "";
		for (int i = 0; i < input.length; i++) {
			_input += input[i];
		}

		return _input;
	}

	// Must be called from the event-dispatching thread.
	protected void resetFocus() {
		passwordField.requestFocusInWindow();
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked
	 * from the event-dispatching thread.
	 */
	public static JFrame createAndShow(JFrame _jframe) {
		// Make sure we have nice window decorations.
		JFrame.setDefaultLookAndFeelDecorated(false);

		// Create and set up the window.
		// JFrame frame = new JFrame("Passwort eingabe");
		// Create and set up the content pane.
		final CheckPassword newContentPane = new CheckPassword();
		newContentPane.setOpaque(false); // content panes must be opaque
		_jframe.setContentPane(newContentPane);

		// Make sure the focus goes to the right component
		// whenever the frame is initially given the focus.
		_jframe.addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				newContentPane.resetFocus();
			}
		});
		// Display the window.
		_jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_jframe.pack();

		return _jframe;
		// _jframe.setVisible(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
	    g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(),null);
	    
	    this.paintComponents(g);
	}
}
