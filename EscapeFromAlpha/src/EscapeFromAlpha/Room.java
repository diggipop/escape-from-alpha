package EscapeFromAlpha;


import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Room {
	String name;
	String textOnObserve;
	Image backgroundImage = null;
	ArrayList<Omi> containedOmis = new ArrayList<Omi>();
	ArrayList<Room> connections = new ArrayList<Room>();
	Boolean observed = false;
	String pathToMusic = null; /* OBSOLETE ? */
	
	Room(String roomName, String textOnObserve, String pathToImage) {
		this(roomName,textOnObserve,pathToImage,null);
	}
	
	Room(String roomName, String textOnObserve, String pathToImage, String pathToMusic) {
		this.textOnObserve = textOnObserve;
		this.name = roomName;
		this.pathToMusic = pathToMusic;
		try {
			this.backgroundImage = ImageIO.read(GameWindow.class.getResource(pathToImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	public void setBackgroundImage(Image image) {
		backgroundImage = image;
	}
	
	public void removeItemFromRoom(Item item) {
		for(Omi omi : containedOmis) {
			omi.containedItems.remove(item);
		}
	}
	
	public int countConnections() {
		return connections.size();
	}
	
	public int countOmis() {
		int c = 0;
		for(Omi omi : this.containedOmis) {
			c++;
		}
		return c;
	}
	
	public int countOmisToUse() {
		int c = 0;
		for(Omi omi : this.containedOmis) {
			if(omi.observed && omi.showForUse)
				c++;
		}
		return c;
	}	
	
	public int countItems() {
		int c = 0;
		for(Omi omi : containedOmis) {
			if(omi.observed) {
				for(Item item : omi.containedItems) {
					c++;
				}
			}
		}
		return c;
	}
	
	public void addOmi(Omi omi) {
		containedOmis.add(omi);
	}
	
	public void removeOmi(Omi omi) {
		containedOmis.remove(omi);
	}
}