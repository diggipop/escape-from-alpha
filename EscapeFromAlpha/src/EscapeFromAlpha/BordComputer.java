package EscapeFromAlpha;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

class BordComputer extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	private Image backgroundImage;

	String textb;
	String textc;
	String texte;
	String textf;

	JButton b;
	JButton c;
	JButton e;
	JButton h;
	JButton i;
	Insets insets;
	Dimension size;

	final JTextPane field;

	// frame
	static JPanel f;
	private JLayeredPane layeredPane;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.repaint();
	}

	BordComputer() {
		this.setOpaque(false);
		setBackground(new Color(1, 0, 0, 0));
		setForeground(Color.white);
		setBorder(new EmptyBorder(50, 50, 50, 50));

		f = this;
		Font font = new Font("Arial", Font.PLAIN, 22);
		setLayout(new BorderLayout(0, 0));

		field = new JTextPane();
		field.setText(textf);
		field.setBackground(new Color(0, 0, 0, 0.8f));
		add(field, BorderLayout.CENTER);
		field.setFont(font);
		field.setForeground(Color.white);
		field.setEditable(false);
		//field.setLineWrap(true);
		//field.setWrapStyleWord(true);

		StyledDocument doc = field.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		
		if (GameLogic.currentGame.currentRoom != null)
			backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;

		field.setText("Der Computer erwartet deine Anfrage.");

		layeredPane = new JLayeredPane();
		layeredPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		add(layeredPane, BorderLayout.SOUTH);
		layeredPane.setLayout(new BoxLayout(layeredPane, BoxLayout.Y_AXIS));
		i = new JButton("empty");
		layeredPane.add(i);
		i.setAlignmentX(Component.CENTER_ALIGNMENT);
		i.addActionListener(this);
		i.setText("Computer, wo sind alle?");
		h = new JButton("empty");
		layeredPane.add(h);
		h.setAlignmentX(Component.CENTER_ALIGNMENT);
		h.addActionListener(this);
		h.setText("Computer, was soll ich tun?");
		e = new JButton("empty");
		layeredPane.add(e);
		e.setAlignmentX(Component.CENTER_ALIGNMENT);
		e.addActionListener(this);
		e.setText("Computer, wieviel Zeit bleibt mir noch?");
		c = new JButton("empty");
		layeredPane.add(c);
		c.setAlignmentX(Component.CENTER_ALIGNMENT);
		c.addActionListener(this);
		c.setText("Computer, wer bin ich?");
		b = new JButton("empty");
		layeredPane.add(b);
		b.setAlignmentX(Component.CENTER_ALIGNMENT);

		// add actionlistener to button
		b.addActionListener(this);
		b.setText("Computer, wo bin ich?");
		b.setVisible(true);
		c.setVisible(true);
		e.setVisible(true);
		h.setVisible(true);

		b.setBackground(new Color(0, 0, 0, 1f));
		b.setForeground(Color.white);

		c.setBackground(new Color(0, 0, 0, 1f));
		c.setForeground(Color.white);

		e.setBackground(new Color(0, 0, 0, 1f));
		e.setForeground(Color.white);

		h.setBackground(new Color(0, 0, 0, 1f));
		h.setForeground(Color.white);

		i.setBackground(new Color(0, 0, 0, 1f));
		i.setForeground(Color.white);

		i.setVisible(true);

		f.setVisible(true);
	}

	public void actionPerformed(ActionEvent event) {
		String s = event.getActionCommand();
		if (s.equals("Computer, wo bin ich?")) {

			field.setText("�Du befindest dich auf Mondbasis Alpha auf am n�rdlichen Rand des Peary Kraters, "
					+ "in der N�he des Nordpols des Mondes.�" + "\n\r\n\r" + "Du befindest dich zur Zeit an folgendem Ort:"
					+ GameLogic.currentGame.currentRoom.name + "�");
			i.setText("Danke.");
			i.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			b.setVisible(false);
		}
		if (s.equals("Computer, wer bin ich?")) {

			field.setText("�Dein Name lautet ...� Statt deinen Namen zu erfahren, h�rst du nur ein Rauschen. "
					+ "�... du bist ein Humanoid. Deine Funktion ist unbekannt. Dein Rang ist unbekannt. FEHLER! FEHLER! FEHLER!�");
			i.setText("Danke.");
			i.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			b.setVisible(false);

		}
		if (s.equals("Computer, wieviel Zeit bleibt mir noch?")) {

			if (GameLogic.currentGame.hasVirus) {
				field.setText(
						"�Die durchschnittliche Lebenserwartung eines Humanoiden mit deinen Biowerten liegt bei drei Monaten.  "
								+ "Du hast noch Sauerstoff f�r f�r "
								+ GameWindow.panelRoom.CalulateTime(DeadTimer.interval) + "�");
			} else {
				field.setText(
						"�Die durchschnittliche Lebenserwartung eines Humanoiden mit deinen Biowerten liegt bei 110 Jahren.  "
								+ "Du hast noch Sauerstoff f�r f�r "
								+ GameWindow.panelRoom.CalulateTime(DeadTimer.interval) + "�");
			}

			i.setText("Danke.");
			i.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			b.setVisible(false);
		}

		if (s.equals("Computer, was soll ich tun?")) {

			field.setText("�Ich kann dir keine Handlungsanweisungen geben, dazu bin ich nicht berechtigt.� ");
			i.setText("Danke.");
			i.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			b.setVisible(false);
		}

		if (s.equals("Computer, wo sind alle?")) {

			field.setText("�Teile der Mannschaft befinden sich auf der Station. "
					+ "Eine genaue Zuordnung ist nicht m�glich. Commander Sisko befindet sich auf der Br�cke.� ");
			i.setText("Danke.");
			i.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			b.setVisible(false);

		} else if (s.equals("Danke.")) {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
		}
	}
}
