package EscapeFromAlpha;

import java.util.ArrayList;

public class Items {
	public static ArrayList<Item> items = new ArrayList<Item>();

	public static Item findItemByName(String name) {
		for (Item item : items) {
			if (item.name.equals(name)) {
				return item;
			}

		}
		System.out.println("ERROR: findItemByName could not find " + name);
		return null;
	}

	public static void applyItemAndOmiCombinations() {
		Omis.getOmiByName("Lüftungsschacht").addItemCombination(
				/* Text bei Kombi. */ "Du rammst den Billardqueue in den Spalt zwischen Wand und Gitter, "
						+ "nutzt ihn geschickt als Hebel und schaffst es letztlich das Gitter zu entfernen. "
						+ "Leider zerbricht der Queue dabei und du lässt ihn zurück.",
				/* Anderes Item */ Items.findItemByName("Billardqueue"), /* Item das entstehen soll */ null,
				/* Sollen Items/Omi gelöscht werden */ true,
				/* Raum zum verbinden / null wenn nicht nötig */ Rooms.getRoomByName("Lagerraum"));

		Omis.getOmiByName("Sandsack").addItemCombination(
				"Mit viel Muskelkraft und dem Messer schneidest du den Sandsack los und sammelst das Seil ein.",
				Items.findItemByName("Messer"), Items.findItemByName("Seil"), true, null, () -> {
					Rooms.getRoomByName(
							"Fitnessraum").textOnObserve = "Dieser Raum scheint vom allgemeinen Chaos nahezu unberührt geblieben zu sein. Trotz roter Notbeleuchtung erkennst du einen typischen Sportraum mit Hantelbänken, Laufbändern, einem kleinen Boxring und dem Sandsack. Du versucht nicht mehr an ihn zu denken.";
				});

		Omis.getOmiByName("Schwere Kiste").addItemCombination(
				"Du bindest das Seil um die Kiste, ziehst so lange, bis sie ein wenig in deine Richtung kippt und bindest das Seil dann an einen Lufthaken fest. Jetzt kannst du den Feuerlöscher nehmen. Direkt nachdem du den Feuerlöscher herausgezogen hast, reißt das Seil und die Kiste kracht zurück auf den Boden. Du erschreckst dich fürchterlich, aber das war es wert.",
				Items.findItemByName("Seil"), Items.findItemByName("Feuerlöscher"), true, null);

		Omis.getOmiByName("Brennendes Alien").addItemCombination(
				"Du löschst das Alien mit dem Feuerlöscher, es bleibt ein kleiner schwarzer Haufen übrig.",
				Items.findItemByName("Feuerlöscher"), null, true, Rooms.getRoomByName("Kommandozentrale"));

		Omis.getOmiByName("Kühlschrank").addItemCombination(
				"Mit dem Besen fischst du das flache Objekt hinter dem Kühlschrank hervor. Es ist ein Tablet, du steckst es ein.",
				// Items.findItemByName("Besen"), Items.findItemByName("Shuttle- Schlüssel"),
				// true, null);
				Items.findItemByName("Besen"), Items.findItemByName("Logbuch 4"), true, null);

		Omis.getOmiByName("Verschlossene Tür des Lagerraumes").addItemCombination(
				"Du steckst die Key-Card in den entsprechenden Slot, die Tür leuchtet hellblau auf und öffnet sich mit einem wohligen Seufzen.",
				Items.findItemByName("Key-Card:Lagerraum"), null, true, Rooms.getRoomByName("Area B"));

		Omis.getOmiByName("Alien").addItemCombination(
				"Du wirfst den Molotov auf das Alien. Es fängt sofort an lichterloh zu brennen, krümmt sich und schreit vor Schmerzen. Es bricht tot vor der Tür zusammen, hört aber nicht auf zu brennen. Der Bordcomputer teilt dir freundlich mit, dass sich der Sauerstoffgehalt der Luft durch das Feuer rapide verringert.",
				Items.findItemByName("Brennender Molotov"), Omis.getOmiByName("Brennendes Alien"), true, null);

		Omis.getOmiByName("Alien").addItemCombination(
				"Du wirfst den Molotov auf das Alien. Es fängt sofort an lichterloh zu brennen, krümmt sich und schreit vor Schmerzen. Es bricht tot vor der Tür zusammen, hört aber nicht auf zu brennen. Der Bordcomputer teilt dir freundlich mit, dass sich der Sauerstoffgehalt der Luft durch das Feuer rapide verringert.",
				Items.findItemByName("Brennender Molotov"), Omis.getOmiByName("Brennendes Alien"), true, null);

		Omis.getOmiByName("Alien").addItemCombination(
				"Du schlägst mit der Axt nach dem Alien, es zerschlägt mühelos den Stiel deiner Waffe und kratzt dabei deine Hand auf. Du flüchtest. Irgendwie fühlst du dich nicht so gut.",
				Items.findItemByName("Axt"), Omis.getOmiByName("Alien"), true, null);
		
		Omis.getOmiByName("Alien").addItemCombination(
				"Du drehst das Radio voll auf und hältst es dem Alien hin. \r\n Das Ungetüm rennt schreiend davon und ward nie wieder gesehen.",
				Items.findItemByName("Geladenes Radio"), null, true, Rooms.getRoomByName("Kommandozentrale"));			

	}

	public Items() {
		Item radioempty = new Item("entladenes Radio",
				"Ein Radio, dass nur Country und Schlager abspielen kann. Zum Glück ist es leer.",
				"Das Radio ist leer und gibt keinen Piep von sich.",
				"Du nimmst das Radio an dich, denn du willst nicht länger mit deinen Gedanken und dem Bordcomputer alleine sein. ",
				true);

		Item energiezelle = new Item("Energiezelle", "Eine universell-nutzbare Einweg-Energiezelle – voll geladen.",
				"Du musst sie irgendwo reinstecken!",
				"In einer Ecke liegt eine Energiezelle. Du hebst sie auf und steckst sie ein.", true);

		Item lampeempty = new Item("Entladene Taschenlampe",
				"Eine praktische Taschenlampe mit dem tollen Feature Licht produzieren zu können. Leider ist sie leer.",
				"Du drückst mehrfach auf den Schalter der Lampe, aber es passiert nichts.",
				"Du nimmst die Taschenlampe an dich, denn es kann viel passieren.", true);

		Item axe = new Item("Axt",
				"Eine wirklich scharfe Axt mit rotem Kopf. An Ihrer Schneide glitzert eine grüne Flüssigkeit.",
				"Du fühlst das Gewicht der Axt und schwingst sie ein paar Mal hin und her. Du fühlst dich gut dabei und denkst. “Hier kommt Johnny“.",
				"Du zerrst die Axt aus der Wand, schleuderst angewidert die Hand von dir und hast nun eine Axt an deiner Seite. Es geht dir gleich viel besser.",
				false);

		Item broom = new Item("Besen", "Ein verlängerter Handfeger also ein stinknormaler Besen.",
				"Nein, du möchtest jetzt und hier nicht putzen.",
				"Du hältst den Dreck nicht mehr aus und nimmst den Besen an dich. Jetzt wirst du hier mal aufräumen!",
				false);

		Item billardqueue = new Item("Billardqueue",
				"Ein sehr stabiler glatter Stock, mit dem man kleine Kugeln rumschubsen kann.", "Ohne Kugeln?",
				"Du nimmst den Queue mit und fühlst dich ziemlich cool dabei.", false);

		Item molotovburning = new Item("Brennender Molotov", "Die klassische Waffe des Randalierers. Heiß!",
				"Du starrst verzaubert auf das Feuer in deiner Hand. Jetzt fehlt dir nur noch ein passendes Ziel.",
				"Du zündest das Tuch an und hast nun einen leckeren Molotov Cocktail.", false);

		Item molotov = new Item("Kalter Molotov", "Eine Flasche Rum mit einem Stück Tuch darin.",
				"Das wäre Verschwendung.",
				"Du stopfst das Tuch in den Flaschenhals. Das hast du mal in einem Film so gesehen.", true);

		Item rum = new Item("Flasche Top-Rum", "Eine Flasche des besten Rums auf dem Mond",
				"Du solltest lieber einen klaren Kopf bewahren.",
				"Du nimmst den Rum an dich. Im Moment ist er dein einziger Freund.", true);

		Item towel = new Item("Tücher", "Allzwecktücher aus selbstreinigendem, ultrasaugfähigem Material.",
				"Du nuckelst an dem Handtuch, aber es enthält keine Nährstoffe mehr. Immerhin weißt du, wo es ist.",
				"Du nimmst ein Tuch mit. Scheinbar bist du ein Messi.", true);

		Item lighter = new Item("Feuerzeug",
				"Ein klassisches Aufklappfeuerzeug einer ehemals bekannten Marke. Es macht Feuer!",
				"Du spielst mit dem Feuerzeug und lässt es klackern. Du kommst dir gut vor dabei.",
				"Du schaust unter das Kissen und findest dort ein Feuerzeug.", true);

		Item rechargelamp = new Item("Geladene Taschenlampe",
				"Eine praktische Taschenlampe mit dem tollen Feature Licht produzieren zu können. Sie ist voll geladen.",
				"Du drückst auf den Schalter der Lampe und ein greller Lichtstrahl erhellt einen großen Bereich vor dir.",
				"Du lädst die Lampe voll auf und verbrauchst dabei die Energiezelle.", false);

		Item rechargeradio = new Item("Geladenes Radio",
				"Ein Radio, dass direkt nach dem Aufladen angefangen hat "
						+ "Country und Schlager abzuspielen. Ob es so klug war, es zu laden?",
				"Du spielst an den Knöpfen rum, aber es ertönt weiterhin die gleiche nervige Musik. Du bereust deine Entscheidung jetzt schon.",
				"Du lädst das Radio auf und verbrauchst dabei die Energiezelle. Es fängt sofort an Schlager zu spielen.",
				false);

		Item extinguisher = new Item("Feuerlöscher", "Löscht zuverlässig jedes Feuer mit aktiv-Löschschaum.",
				"Ohne Feuer?",
				"Direkt nachdem du den Löscher herausgezogen hast, reißt das Seil und die Kiste kracht zurück auf den Boden. "
						+ "Du erschreckst dich fürchterlich, aber das war es wert. ",
				false);
		Item jetpack = new Item("Jet-Pack",
				"Ein echtes Jet-Pack! Wie geil ist das denn? "
						+ "Damit kommt man schnell von der Station zum Raumschiff.",
				"Nicht in geschlossenen Räumen verwenden!",
				"Gierig nimmst du das Jet-Pack an dich und schnallst es dir um. Du solltest es nur außerhalb der Station verwenden.",
				false);

		Item cards = new Item("Kartenspiel",
				"Ein normales Kartenset mit ausgefallenem Design. Wenn man keine Mitspieler hat, "
						+ "kann man auch alleine Patience damit spielen.",
				"Du spielst eine Runde Karten mit dir selbst, um dich zu beruhigen. Leider verlierst du und bist nun noch angespannter.",
				"Du nimmst die Karten an dich. Auf die Essensmarken verzichtest du, weil dir der Appetit gründlich vergangen ist.",
				false);

		Item keycardstorage = new Item("Key-Card:Lagerraum", "Diese Schlüssel-Karte öffnet die Tür des Lagerraums.",
				"Du wedelst dir mit der Schlüsselkarte ein wenig Luft zu.", "Du steckst die Key-Card ein.", false);

		Item ducttape = new Item("Klebeband", "Super-stabiles Space-Tape. Klebt alles! Geht nie wieder ab!",
				"Klebeband hat viele tausend Verwendung. Nur gerade hat es keine. ",
				"In dem leeren Werkzeugkasten entdeckst du noch eine Rolle Klebeband und nimmst sie mit.", true);

		Item knife = new Item("Messer",
				"Ein stinknormales Buttermesser, ganz ohne Sci-Fi-Faktor. "
						+ "Mit viel Mühe kann man damit auch etwas durchschneiden.",
				"Ohne Brot und Butter?", "Du nimmst ein Messer aus einer der Schubladen, warum auch nicht?", false);

		Item spacesuitdamaged = new Item("Raumanzug",
				"Dieser Raumanzug sieht ziemlich mitgenommen aus. Er hat offensichtlich einige Löcher "
						+ "und ist mit diversen Flüssigkeiten beschmiert.",
				"Du steckst einen Finger durch ein Loch in deinem Raumanzug. Das erinnert dich an etwas.",
				"Du löst den Raumanzug vom Gestell und ziehst ihn direkt an. Natürlich passt er dir perfekt.", true);

		Item spacesuit = new Item("Geflickter Raumanzug",
				"Mit Klebeband notdürftig geflickter.Du bist dir aber sicher, dass er dicht ist.",
				"Du schaust an dir herab und fragst dich, ob die Flicken halten werden.",
				"Du versiegelst alle Löcher in deinem Raumanzug mit dem Klebeband und schließt Testweise den Helm. "
						+ "Sofort kommt eine kleine Mitteilung, die dir sagt, dass alles in Ordnung ist.",
				false);

		Item oxygenbottle = new Item("Sauerstoffflasche A",
				"Eine gut gefüllte Flasche mit Sauerstoff, die sich an einen Raumanzug anschließen lässt. Du hast noch nie etwas schöneres gesehen.",
				"Du solltest jetzt keinen Druck ablassen",
				"Du erhältst eine Flasche Sauerstoff, die du nun mit dir rumschleppen musst.", false);

		Item oxygencylinder = new Item("Sauerstoffflasche B",
				"Eine gut gefüllte Flasche mit Sauerstoff, die sich an einen Raumanzug anschließen lässt. Du hast noch nie etwas schöneres gesehen.",
				"Du solltest jetzt keinen Druck ablassen",
				"Du erhältst eine Flasche Sauerstoff, die du nun mit dir rumschleppen musst.", false);

		Item rope = new Item("Seil", "Ein stabiles Seil, gut 5 Meter lang. Gehört in jedes Adventure.",
				"Du springst eine Runde Seil, bis du außer Puste bist. Was hat dir das gebracht?",
				"Mit viel Muskelkraft und dem Messer schneidest du den Sandsack los und sammelst das Seil ein.", false);

		Item shuttlekey = new Item("Shuttle- Schlüssel",
				"Ein irgendwie antiquiert wirkender Schlüsselbund mit 2 kleinen Schlüsselanhängern "
						+ "in Form eines Shuttles und eines roten Herings.",
				"Du bewunderst den roten Hering am Schlüsselbund und fragst dich, welchen Zweck er haben könnte.",
				"Mit dem Besen fischst du das glitzernde Objekt hinter dem Kühlschrank hervor. Es ist ein Schlüsselbund, du steckst ihn ein.",
				false);

		Item log2 = new Item("Logbuch 2",
				"Das Logbuch lässt sich nicht mehr bedienen und zeigt nur einen kurzen Tagebucheintrag ein.\r\n"
						+ "29072119\r\n"
						+ "Die Pflanzen gedeihen viel besser als gedacht. Die künstliche Atmosphäre und das spezielle Licht scheinen sich gut auf das Wachstum auszuwirken. Die Jungs haben sich einen Spaß erlaubt und eine „Vogelscheuche“ aufgestellt. Als ob der sprechende Busch nicht schon genug Gesellschaft gewesen wäre. Aber ich bin ihnen nicht böse. Hab das Passwort für die Schleuse für diese Woche schon wieder vergessen, irgendwas mit A am Ende. Ist ja auch nicht so wichtig. Das Leben ist hier drinnen oder so ;) \r\n"
						+ "\r\n" + "Nyota\r\n",
				"Du schaust dir das beschädigte Tablet näher an und stellst fest, dass es ein Logbuch ist. Spannend!",
				"Du hebst ein trüb leuchtendes Logbuch vom Boden auf.", false);

		Item log1 = new Item("Logbuch 1", "Das Logbuch ist beschädigt und nur noch wenige Fragmente sind lesbar.  \r\n"
				+ "… Außenhülle beschädigt … ist es so hereingekommen? ….  Es ist jetzt im hinteren Flur, haben Türen versiegelt und …. Maßnahmen … \r\n"
				+ "Warten seit Tagen…. muss schlafen…..hier raus!! … nicht vergessen: L…. \r\n" + "\r\n"
				+ "D. Troy\r\n",
				"Du schaust dir das beschädigte Tablet näher an und stellst fest, dass es ein Logbuch ist. Spannend!",
				"Du hebst ein trüb leuchtendes Logbuch vom Boden auf.", false);

		Item log3 = new Item("Logbuch 3",
				"Das Logbuch hat einen Sprung auf dem Display und ist mit Blut verkrustet. Zwischen den Flecken kannst du folgendes lesen: \r\n"
						+ "… nun mal die Mission….woher sollten wir wissen, dass es sie anlockt? ... niemals unbewaffnet herkommen sollen… Troy hat es erwischt…Schächte………..Scheiß auf die anderen….Schleuse…..verdammter Code …U…\r\n"
						+ "\r\n" + "M. Chief\r\n",
				"Du schaust dir das beschädigte Tablet näher an und stellst fest, dass es ein Logbuch ist. Spannend!",
				"Du hebst ein trüb leuchtendes Logbuch vom Boden auf.", false);

		Item log4 = new Item("Logbuch 4",
				"Ein schwer beschädigtes Logbuch. Über das Display flackern einzelne Wort und Buchstaben. \r\n"
						+ "…Chance…ist allein…trotzdem…Schande…hab es erwischt…auch…Mist…Shuttle .…..N..\r\n" + "\r\n"
						+ "Auf er Rückseite klebt ein Sticker: S. Spiegel.\r\n",
				"Du schaust dir das beschädigte Tablet näher an und stellst fest, dass es ein Logbuch ist. Spannend!",
				"Du hebst ein trüb leuchtendes Logbuch vom Boden auf.", false);

		energiezelle.addItemCombination(lampeempty, rechargelamp, true);
		energiezelle.addItemCombination(radioempty, rechargeradio, true);
		rum.addItemCombination(towel, molotov, true);

		lighter.addItemCombination(molotov, molotovburning, true);

		ducttape.addItemCombination(spacesuitdamaged, spacesuit, true);

		items.add(axe); // 0
		items.add(broom); // 1
		items.add(billardqueue); // 2
		items.add(molotovburning); // 3
		items.add(molotov); // 4
		items.add(rum); // 5
		items.add(towel); // 6
		items.add(lighter); // 7
		items.add(rechargelamp); // 8
		items.add(rechargeradio); // 9
		items.add(extinguisher); // 10
		items.add(jetpack); // 11
		items.add(cards); // 12
		items.add(keycardstorage); // 13
		items.add(ducttape); // 14
		items.add(knife); // 15
		items.add(spacesuitdamaged); // 16
		items.add(spacesuit); // 17
		items.add(oxygenbottle); // 18
		items.add(oxygencylinder); // 19
		items.add(rope); // 20
		items.add(shuttlekey); // 21
		items.add(log2); // 22
		items.add(log1); // 23
		items.add(log3); // 24
		items.add(log4); // 25

		items.add(energiezelle);
		items.add(rum);
		items.add(radioempty);
		items.add(lampeempty);

	}
}