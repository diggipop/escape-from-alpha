package EscapeFromAlpha;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class DialogAutomat extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	private Image backgroundImage;

	String textb;
	String textc;
	String texte;
	String textf;

	JButton b;
	JButton c;
	JButton e;
	JButton h;
	JButton i;
	Insets insets;
	Dimension size;

	final JTextPane field;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.repaint();
	}

	// frame
	static JPanel f;
	private JLayeredPane layeredPane;

	DialogAutomat() {
		this.setOpaque(false);
		setBackground(new Color(1, 0, 0, 0));
		setForeground(Color.white);
		setBorder(new EmptyBorder(50, 50, 50, 50));
		// create a new frame
		f = this;

		// create a object
		DialogAutomat s = this;

		// Variablen f�r text
		textb = "Ja";
		textc = "Nein";
		textf = "Hallo du Held, bist du gekommen, um dich auf einen Mondspaziergang vorzubereiten?";

		// panel im panel
		Font font = new Font("Arial", Font.PLAIN, 22);
		setLayout(new BorderLayout(0, 0));

		JPanel inneres = new JPanel();
		inneres.setVisible(true);

		field = new JTextPane();
		field.setText(textf);
		field.setBackground(new Color(0, 0, 0, 0.8f));
		field.setFont(font);
		field.setForeground(Color.white);
		field.setEditable(false);
		//field.setLineWrap(true);
		//field.setWrapStyleWord(true);
		
		StyledDocument doc = field.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);		
		
		add(field, BorderLayout.CENTER);

		layeredPane = new JLayeredPane();
		layeredPane.setLayout(new BoxLayout(layeredPane, BoxLayout.Y_AXIS));
		add(layeredPane, BorderLayout.SOUTH);

		b = new JButton(textb);
		b.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(b);
		c = new JButton(textc);
		c.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(c);
		e = new JButton(texte);
		e.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(e);
		h = new JButton("");
		h.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(h);
		i = new JButton("");
		i.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(i);

		e.setVisible(false);
		h.setVisible(false);
		i.setVisible(false);

		i.addActionListener(s);
		h.addActionListener(s);
		e.addActionListener(s);
		c.addActionListener(s);
		b.addActionListener(s);
		
		b.setBackground(new Color(0,0,0,1f));
		b.setForeground(Color.white);

		c.setBackground(new Color(0,0,0,1f));
		c.setForeground(Color.white);

		e.setBackground(new Color(0,0,0,1f));
		e.setForeground(Color.white);

		h.setBackground(new Color(0,0,0,1f));
		h.setForeground(Color.white);
		
		i.setBackground(new Color(0,0,0,1f));
		i.setForeground(Color.white);	

		insets = b.getInsets();

		if (GameLogic.currentGame.currentRoom != null)
			backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;

		f.setVisible(true);
	}

	public void actionPerformed(ActionEvent event) {
		String s = event.getActionCommand();

		if (s.equals("Ja") || s.equals("Auf gar keinen Fall!") || s.equals("Brauche ich nicht.") || s.equals("Roger.")
				|| s.equals("Noch nicht.")) {
			field.setText("M�chtest du a) eine Brosch�re �ber die Sch�nheit der Krater auf der Mondoberfl�che?"
					+ " \nM�chtest du b) eine Sauerstoffflasche f�r deinen Raumanzug?"
					+ " Oder\nm�chtest du c) die Schleusent�r �ffnen?");
			b.setText("a)");
			c.setText("b)");
			e.setText("c)");
			
			b.setVisible(true);
			c.setVisible(true);
			e.setVisible(true);
			i.setVisible(false);
			h.setVisible(false);
		}

		if (s.equals("a)")) {
			field.setText("M�chtest du eine Brosch�re �ber die Sch�nheit der Krater auf der Mondoberfl�che?");
			b.setText("Unbedingt!");
			c.setText("Auf gar keinen Fall!");
			
			b.setVisible(true);
			c.setVisible(true);
			e.setVisible(false);
			i.setVisible(false);
			h.setVisible(false);
		}

		if (s.equals("Unbedingt!")) {
			field.setText("FEHLER: Keine Brosch�ren mehr verf�gbar! ");
			b.setText("Schade.");
			
			b.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			i.setVisible(false);
			h.setVisible(false);
		}

		if (s.equals("b)")) {
			field.setText("M�chtest du eine Sauerstoffflasche f�r deinen Raumanzug?");
			if (!Inventar.isinInv("Sauerstoffflasche A")) {
				b.setText("Das will ich.");
			} else {
				b.setVisible(false);
			}

			c.setText("Brauche ich nicht.");
			c.setVisible(true);
			e.setVisible(false);
			i.setVisible(false);
			h.setVisible(false);
		}

		if (s.equals("Das will ich.")) {
			field.setText(
					"Der Automat stellt dir eine Sauerstofflasche zur Verf�gung und w�nscht dir noch einen sch�nen Tag. ");
			b.setText("Nimm Sauerstoffflasche.");
			
			b.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			i.setVisible(false);
			h.setVisible(false);
		}

		if (s.equals("c)")) {
			field.setText("M�chtest du die Schleusent�r �ffnen?");
			b.setText("Na klar.");
			c.setText("Noch nicht.");
			
			b.setVisible(true);
			c.setVisible(true);
			e.setVisible(false);
			i.setVisible(false);
			h.setVisible(false);
		}

		if (s.equals("Na klar.")) {
			field.setText("Gib dazu einfach an der Konsole neben der Schleusent�r das Passwort ein.");
			b.setText("Roger.");
			
			b.setVisible(true);
			c.setVisible(false);
			e.setVisible(false);
			i.setVisible(false);
			h.setVisible(false);
		}

		if (s.equals("Nein") || s.equals("Schade.") || s.equals("Nimm Sauerstoffflasche.")) {
			GameWindow.currentWindow.panelRoom.setText(GameLogic.currentGame.currentRoom.textOnObserve);
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
		}

		if (s.equals("Nimm Sauerstoffflasche.")) {
			Inventar.addToInv(Items.findItemByName("Sauerstoffflasche A"));
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
		}
	}
}