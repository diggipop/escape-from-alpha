package EscapeFromAlpha;

import java.util.ArrayList;

public class Inventar {

	static ArrayList<BaseItem> myInv = new ArrayList<BaseItem>();

	public static void addToInv(BaseItem item) {
		myInv.add(item);
		GameWindow.panelRoom.setDisplayedInventoryText(getInventarAsString());
	}

	public static void removeFromInv(BaseItem item) {
		System.out.println("Remove " + item.name + " : " + myInv.remove(item));
		GameWindow.panelRoom.setDisplayedInventoryText(getInventarAsString());
	}

	public static String[] displayInv() {
		String[] content = new String[myInv.size()];
		for (int i = 0; i < content.length; i++) {
			content[i] = myInv.get(i).name;
			System.out.println(content[i]);
		}
		return content;
	}
	
	public static String getInventarAsString() {
		String r = "";
		for (int i = 0; i < myInv.size(); i++) {
			r += " | " + myInv.get(i).name;
		}
		return r;
	}

	public static boolean isinInv(String item) {
		for (int i = 0; i < myInv.size(); i++) {
			if (item.equals(myInv.get(i).name)) {
				return true;
			}
		}
		return false;
	}

	//public static void main(String[] args) {
	//	Item radioempty = new Item("entladenes Radio",
	//			"Ein Radio, dass nur Country und Schlager abspielen kann. Zum Gl�ck ist es leer.", "radiouse",
	//			"Du nimmst das Radio an dich, denn du willst nicht l�nger mit deinen Gedanken und dem Bordcomputer alleine sein.",
	//			true);

	//	Item energiezelle = new Item("Energiezelle", "Eine universell-nutzbare Einweg-Energiezelle voll geladen.",
	//			"energiezelleuse", "energiezellepickup", true);

	//	Inventar.addToInv(radioempty);
	//	Inventar.isinInv("Energiezelle");
	//	System.out.println(isinInv("entladenes Radio"));
	//	Inventar.displayInv();
	//}
}
