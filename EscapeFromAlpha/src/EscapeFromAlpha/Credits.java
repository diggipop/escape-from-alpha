package EscapeFromAlpha;

import java.awt.BorderLayout;

import java.awt.Color;

import java.awt.Font;

import java.awt.event.MouseEvent;

import java.awt.event.MouseListener;

import javax.swing.*;

@SuppressWarnings("serial")

public class Credits extends JPanel implements MouseListener {

	static Font _font = new Font("Lucida Console", Font.BOLD, 12);

	static int x = 0;

	static String font = "Arial";

	static boolean exit = false;

	private static int[] _spaces;

	private static String[] credits = {
			"*-._,-'\"`-._,-'\"`-._,-'\"`-._,-*",
			
			" ",
			
			"CREDITS",
			
			" ",
			
			"~.-=o=-.~",
			
			" ",

			"LEAD GAME DEVELOPERS",

			"Thomas Bachmann",

			"Stefan Laufer",

			"Khanh Ngac",

			"Sebastian Skop",

			"Jens Wendt",

			"Diggi Pop",
			
			" ",
			
			"LEAD SOUND DESIGNER",
			
			"Diggi Pop",
			
			" ",

			"LEAD PROJECT MANAGERS",
			
			"Thomas Bachmann",

			"Sebastian Skop",

			"Jens Wendt",

			"Khanh Ngac",

			"Diggi Pop",

			" ",

			"LEAD DESIGNER",
			
			"Stefan Laufer",
			
			" ",

			"LEAD PLANNERS",
			
			"",
			
			"Thomas Bachmann",

			"Stefan Laufer",
			
			" ",

			"LEAD LEADER",
			
			"Thomas Bachmann",
			
			" ",

			"SPECIAL THANKS TO:",
			
			"Ren�",
			
			"Alien (RIP)",

			"KLAUS (RIP)",

			"Bordcomputer",

			"IAD - Erfurt",
			
			" ",

			"Made as a project",
			
			"within one week",
			
			"at a java-workshop - BAAM!",
			
			" ",
			
			"Powered by JAVA, ECLIPSE",

			"\u00a9 2019 Erfurt, Germany",
			
			" ",
			
			"Thank you for playing our game",
			
			"More infos at",
			
			"EscapeFromAlpha@quantentunnel.de",
			
			
			"*-._,-'\"`-._,-'\"`-._,-'\"`-._,-*"};

	public Credits() {

		addMouseListener(this);

		_spaces = new int[credits.length];

		GetMaxLength();

	}
/**
 * Leerzeichen vor Text für Mitte
 */
	void GetMaxLength() {

		for (int i = 0; i < credits.length; i++) {

			_spaces[i] = (30 - credits[i].length()) / 2;

		}

	}

	/**
	 * Leerzeichen vor Text für Mitte In String bauen
	 * @param i
	 * @return
	 */
	static String getSpaces(int i) {

		String _string = "";

		for (int c = 0; c < _spaces[i]; c++) {

			_string += " ";

		}

		return _string;

	}

	/**
	 * 
	 * 
	 * 
	 * @param f
	 * 
	 */

	public static void Start(JFrame f) {

		f.setLayout(new BorderLayout());

		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JTextArea jta = new JTextArea("von:", 55, 34);

		jta.setEditable(false); // This must not be editable

		jta.setFont(_font);

		jta.setBackground(Color.BLACK);

		jta.setForeground(Color.WHITE);

		final JScrollPane jsp = new JScrollPane(jta);

		//

		final Thread t = new Thread(new Runnable() {

			public void run() {

				while (!exit) {

					final StringBuilder sb = new StringBuilder();

					for (int i = 0; i < credits.length; i++) {

						//System.out.println(credits[i]);

						sb.append(getSpaces(i) + credits[i]);

						// new line

						sb.append("\n");

						// render method

						render(jta, f, jsp, sb);

						// delay

						try {
							Thread.sleep(550);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

					}

					Exit();

					SwingUtilities.invokeLater(new Runnable() {

						public void run() {

							jta.setText(sb.toString());

						}

					});

				}

			}

		});

		t.start();

	}
	/**
	 * neu rendern
	 * @param jta
	 * @param f
	 * @param jsp
	 * @param sb
	 */
	static void render(JTextArea jta, JFrame f, JScrollPane jsp, StringBuilder sb) {

		jta.setText(sb.toString() + "\n");

		f.add(jsp, BorderLayout.CENTER);

		f.pack();

		f.setLocationRelativeTo(null);

		f.setVisible(true);

	}

	@Override

	public void mouseClicked(MouseEvent e) {

		Exit();

	}

	@Override

	public void mouseEntered(MouseEvent e) {

	}

	@Override

	public void mouseExited(MouseEvent e) {

	}

	@Override

	public void mousePressed(MouseEvent e) {

		Exit();

	}

	@Override

	public void mouseReleased(MouseEvent e) {

	}

	/*
	 * Ende Programm
	 */
	static void Exit() {

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		exit = true;

		System.out.println("Danke f�rs spielen");

		System.exit(0);

	}

}
