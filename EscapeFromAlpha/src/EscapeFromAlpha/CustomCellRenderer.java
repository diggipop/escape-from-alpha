package EscapeFromAlpha;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

public class CustomCellRenderer extends DefaultListCellRenderer {
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        if ( ((CustomListEntry)value).isInInventory ) {
        	c.setForeground(new Color(0,166,255,255));
        } else {
        	c.setForeground(Color.white);
        }
        
        if ( ((CustomListEntry)value).isObserved ) {
        	c.setFont(c.getFont().deriveFont(Font.PLAIN));
        } else {
        	c.setFont(c.getFont().deriveFont(Font.BOLD));
        }
        return c;
    }
}
