package EscapeFromAlpha;

//java Program to create a dialog within a dialog 
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

class Alien extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Image backgroundImage;

	String textb;
	String textc;
	String texte;
	String textf;

	JButton b;
	JButton c;
	JButton e;
	JButton h;
	JButton i;
	Insets insets;
	Dimension size;

	final JTextPane field;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);
		this.repaint();
	}

	// frame
	static JPanel f;
	private JLayeredPane layeredPane;
	Alien() {
		this.setOpaque(false);
		setBackground(new Color(1, 0, 0, 0));
		setForeground(Color.white);
		setBorder(new EmptyBorder(50, 50, 50, 50));
		// create a new frame
		f = this;

		// create a object
		Alien s = this;

		// Variablen f�r text
		textb = "Ich bin hier um dich zu t�ten!";
		textc = "Ich will reden!";
		texte = "Ich habe es mir anders �berlegt.";
		textf = "Das Alien zischt dich an, das Zischen klingt fast wie eine Sprache. "
				+ "Nachdem du dem Zischen aus sicherer Distanz ein wenig zugeh�rt hast, beginnst du zu verstehen. "
				+ "Es fragt: �Was willst du?�.";

		// panel im panel
		Font font = new Font("Arial", Font.PLAIN, 22);
		setLayout(new BorderLayout(0, 0));
		
		JPanel inneres = new JPanel();
		inneres.setVisible(true);
		
		field = new JTextPane();
		field.setBackground(new Color(0, 0, 0, 0.8f));
		field.setFont(font);
		field.setForeground(Color.white);
		field.setEditable(false);
		field.setText(textf);
		//field.setLineWrap(true);
		//field.setWrapStyleWord(true);
		
		StyledDocument doc = field.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		
		
		add(field, BorderLayout.CENTER);

		layeredPane = new JLayeredPane();
		layeredPane.setLayout(new BoxLayout(layeredPane, BoxLayout.Y_AXIS));
		add(layeredPane, BorderLayout.SOUTH);
		
		b = new JButton(textb);
		b.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(b);
		c = new JButton(textc);
		c.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(c);
		e = new JButton(texte);
		e.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(e);
		h = new JButton("");
		h.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(h);
		i = new JButton("");
		i.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(i);
		
		i.addActionListener(s);
		i.setVisible(false);
		h.addActionListener(s);
		h.setVisible(false);
		e.addActionListener(s);
		c.addActionListener(s);
		b.addActionListener(s);
			
		b.setBackground(new Color(0,0,0,1f));
		b.setForeground(Color.white);

		c.setBackground(new Color(0,0,0,1f));
		c.setForeground(Color.white);

		e.setBackground(new Color(0,0,0,1f));
		e.setForeground(Color.white);

		h.setBackground(new Color(0,0,0,1f));
		h.setForeground(Color.white);
		
		i.setBackground(new Color(0,0,0,1f));
		i.setForeground(Color.white);		
		
		insets = b.getInsets();
		
		if (GameLogic.currentGame.currentRoom != null)
			backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
		
		f.setVisible(true);
	}

	public void actionPerformed(ActionEvent event) {
		String s = event.getActionCommand();
		if (s.equals("Ich bin hier um dich zu t�ten!")) {
			field.setText("Womit m�chtest du das Alien t�ten?");
			b.setText("Mit blo�en H�nden!"); // Funktion Game OVER
			
			// Abfrage: Axt im Inventar
			if(Inventar.isinInv("Axt")) {
				c.setText("Mit meiner Axt!"); 
				c.setVisible(true);
			} else { c.setVisible(false); }
			
			// Abfrage: Messer im Inventar
			if(Inventar.isinInv("Messer")) {
				e.setText("Mit diesem Messer!");
				e.setVisible(true);
			} else { e.setVisible(false); }
			
			// Abfrage: brennender Molotov im Inventar
			if(Inventar.isinInv("Brennender Molotov")) {
				h.setText("Mit Feuer!");
				h.setVisible(true);
			} else { h.setVisible(false); }
			
			// Abfrage: geladenes Radio im Inventar
			if (Inventar.isinInv("Geladenes Radio")) {
				i.setText("Mit schlechter Musik!"); 
				i.setVisible(true);
			};
		}
		
		if (s.equals("Mit blo�en H�nden!") || s.equals("Mit diesem Messer!")) {
			GameWindow.currentWindow.switchScene(Scenes.GameOverAlien1);
		}
		
		if (s.equals("Mit meiner Axt!")) {
			field.setText("Du schl�gst mit der Axt nach dem Alien, es zerschl�gt m�helos den Stiel deiner Waffe und kratzt dabei deine Hand auf. Du fl�chtest. Irgendwie f�hlst du dich nicht so gut.");
			
			Inventar.removeFromInv(Items.findItemByName("Axt"));
			GameLogic.currentGame.hasVirus = true;

			b.setText("Fl�chten");
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			i.setVisible(false);
		}
		
		if (s.equals("Mit Feuer!")) {
			field.setText("Du wirfst den Molotov auf das Alien. Es f�ngt sofort an lichterloh zu brennen, kr�mmt sich und schreit vor Schmerzen. Es bricht tot vor der T�r zusammen, h�rt aber nicht auf zu brennen. Der Bordcomputer teilt dir freundlich mit, dass sich der Sauerstoffgehalt der Luft durch das Feuer rapide verringert.");
			
			Inventar.removeFromInv(Items.findItemByName("Brennender Molotov"));
			GameLogic.currentGame.currentRoom.containedOmis.remove(Omis.getOmiByName("Alien"));
			GameLogic.currentGame.currentRoom.addOmi(Omis.getOmiByName("Brennendes Alien"));
			DeadTimer.interval = DeadTimer.interval - (60 * 20);
			Omis.getOmiByName("Flur-Boden").textOnObserve = "Ein langer d�mmeriger Flur. Die W�nde wirken fleckig, an einigen Stellen kannst du Besch�digungen erkennen. Durch das Halbdunkel leuchten die T�rschilder �ber den verschiedenen T�ren, die von dem Flur abzweigen. Du erkennst acht Schilder: Quartiere, WC, Erholung, Kantine, Gym, G�rten, Br�cke und Area B.  Sowie die verkohlten �berreste des Aliens.";
			
			b.setText("Einen Schritt zur�ck machen");
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			i.setVisible(false);
		}
		
		if (s.equals("Mit schlechter Musik!")) {
			field.setText("Du drehst das Radio voll auf und h�ltst es dem Alien hin. "
					+ "Das Unget�m rennt schreiend davon und ward nie wieder gesehen.");
			
			GameLogic.currentGame.currentRoom.containedOmis.remove(Omis.getOmiByName("Alien"));
			GameLogic.currentGame.currentRoom.connections.add(Rooms.getRoomByName("Kommandozentrale"));
			Omis.getOmiByName("Flur-Boden").textOnObserve = "Ein langer d�mmeriger Flur. Die W�nde wirken fleckig, an einigen Stellen kannst du Besch�digungen erkennen. Durch das Halbdunkel leuchten die T�rschilder �ber den verschiedenen T�ren, die von dem Flur abzweigen. Du erkennst acht Schilder: Quartiere, WC, Erholung, Kantine, Gym, G�rten, Br�cke und Area B.  Irgendwelche Aliens sind nicht mehr zu sehen.";
			
			b.setText("Einen Schritt zur�ck machen");
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			i.setVisible(false);
		}		
		
		if (s.equals("Ich will reden!")) {
			field.setText("Was m�chtest du dem Alien sagen?");
			b.setText("Lass mich bitte in diesen Raum.");
			c.setText("Was ist hier passiert?");
			e.setText("Deine Mutter wurde von Ripley ...");
			i.setText("Gespr�ch beenden");
			i.setVisible(true);
		}
		
		if (s.equals("Lass mich bitte in diesen Raum.")) {
			field.setText("Das Alien schnappt nach dir, schnell machst du einen Schritt zur�ck!");
			b.setText("Einen Schritt zur�ck machen");
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			i.setVisible(false);
		}
		if (s.equals("Was ist hier passiert?")) {
			field.setText("Dieser Ort geh�rt mir! Ihr seid Beute!");
			b.setText("Einen Schritt zur�ck machen");
			c.setVisible(false);
			e.setVisible(false);
			h.setVisible(false);
			i.setVisible(false);

		}
		if (s.equals("Deine Mutter wurde von Ripley ...")) {
			GameWindow.currentWindow.switchScene(Scenes.GameOverAlien2);
		}

		if (s.equals("Gespr�ch beenden")) {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			GameWindow.currentWindow.panelRoom.setText( GameLogic.currentGame.currentRoom.textOnObserve );

		} else if (s.equals("Ich habe es mir anders �berlegt.")) {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			GameWindow.currentWindow.panelRoom.setText( GameLogic.currentGame.currentRoom.textOnObserve );
		}
		if (s.equals("Fl�chten")) {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			GameWindow.currentWindow.panelRoom.setText( GameLogic.currentGame.currentRoom.textOnObserve );
		}
		
		if (s.equals("Einen Schritt zur�ck machen")) {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			GameWindow.currentWindow.panelRoom.setText( GameLogic.currentGame.currentRoom.textOnObserve );
		}		
	}
}
