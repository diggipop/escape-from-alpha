package EscapeFromAlpha;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PanelUseList extends JPanel {
	private static final long serialVersionUID = 1L;
	private Image backgroundImage;
	private DefaultListModel<CustomListEntry> data = new DefaultListModel<CustomListEntry>();
	private JList<CustomListEntry> list;

	public void update() {
		backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
		data.removeAllElements();

		data.addElement(new CustomListEntry(Omis.getOmiByName("Board-Computer").name, false, true));

		for (Omi omi : GameLogic.currentGame.currentRoom.containedOmis) {
			if (omi.showForUse && omi.observed)
				data.addElement(new CustomListEntry(omi.name, false, omi.observed));
		}

		for (BaseItem item : Inventar.myInv) {
			data.addElement(new CustomListEntry(item.name, true, item.observed));
		}
	}

	public PanelUseList() {
		setBackground(Color.GREEN);
		setBorder(new EmptyBorder(50, 150, 50, 150));
		setLayout(new BorderLayout(0, 0));

		list = new JList<CustomListEntry>(data);
		list.setFont(new Font("Arial", Font.PLAIN, 12));
		list.setForeground(Color.WHITE);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (list.getSelectedIndex() >= 0) {
					GameLogic.currentGame.useListSelectionChanged(((CustomListEntry) list.getSelectedValue()).text);
				}
			}
		});
		list.setBackground(new Color(0, 0, 0, .5f));
		list.setCellRenderer(new CustomCellRenderer());
		add(list, BorderLayout.CENTER);

		JTextPane header = new JTextPane();
		header.setForeground(Color.WHITE);
		header.setFont(new Font("Arial", Font.PLAIN, 22));
		header.setBackground(new Color(0, 0, 0, .5f));
		header.setEditable(false);
		header.setText("Was m�chtest du benutzen bzw. mit wem m�chtest du sprechen?");
		add(header, BorderLayout.NORTH);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.paintComponents(g);
	}
}