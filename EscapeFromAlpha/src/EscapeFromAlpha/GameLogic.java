package EscapeFromAlpha;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import javazoom.jl.decoder.JavaLayerException;

public class GameLogic {
	static GameLogic currentGame;

	Boolean gameStarted = false;
	Boolean hasVirus = false;
	Room currentRoom;
	DeadTimer timer;

	GameLogic() {
		GameLogic.currentGame = this;
		new Items();
		new Omis();
		new Rooms();

		Items.applyItemAndOmiCombinations();

		new BaseItems();
	}

	void startGame() {
		System.out.println("Game started");

		gameStarted = true;
		timer = new DeadTimer("5400"); //1,5h

		GameWindow.currentWindow.switchScene(Scenes.Game);

		gotoRoom(Rooms.getRoomByName("WC"),
				"Du erwachst sitzend und mit starken Kopfschmerzen, das flackernde Licht und die allgemeine D�sternis tragen nicht zu deinem Wohlbefinden bei. Als du dich noch fragst, wo du bist, ert�nt eine laute Computerstimme �ber dir, die sachlich folgendes mitteilt: �Starke Besch�digung des Sauerstofftanks festgestellt. Gesch�tzte Zeit bis zum vollst�ndigen Verbrauch der Sauerstoffreserven: noch 1.5 Stunden. Besch�digung der Au�enh�lle festgestellt. Die Notprotokolle wurden eingeleitet. Bitte verlassen Sie die Station.� Du wei�t nicht, wo du bist, du wei�t nicht, wer du bist aber du wei�t, dass du hier weg musst! GET READY PLAYER ONE");
	}

	void gotoRoom(Room room) {
		if (room.observed) {
			gotoRoom(room, room.textOnObserve);
		} else {
			gotoRoom(room, "Du betrittst " + room.name);
		}

	}

	void gotoRoom(Room room, String enterText) {
		currentRoom = room;
		GameWindow.panelRoom.setText(enterText);
		GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);

		if (currentRoom.observed)
			GameWindow.panelButtons.renameObserveButton("Ansehen");
		else
			GameWindow.panelButtons.renameObserveButton("Erkunden");
	}

	// Region Timer-Callback
	public void timerEnded() {
		GameWindow.currentWindow.switchScene(Scenes.GameOver1);
	}

	public void timerChanged(int timeleft) {
		GameWindow.panelRoom.setDisplayedTime(timeleft);
	}
	// EndRegion

	// Region Listen-Callbacks
	void gotoListSelectionChanged(String roomname) {
		if (currentRoom.name == roomname)
			return;

		gotoRoom(Rooms.getRoomByName(roomname));
	}

	void observeListSelectionChanged(String name) {
		GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);

		BaseItem bi = BaseItems.getByName(name);
		bi.observed = true;
		GameWindow.panelRoom.setText(bi.textOnObserve);

		// Spezial-F�lle
		switch (bi.name) {
		case "Georg W. Bush":
			currentRoom.addOmi(Omis.getOmiByName("L�ftungsschacht"));
			currentRoom.removeOmi(Omis.getOmiByName("Georg W. Bush"));
			break;
		case "Tische mit Schublade":
			currentRoom.removeOmi(Omis.getOmiByName("Tische mit Schublade"));
			currentRoom.addOmi(Omis.getOmiByName("Schublade"));
			break;
		}
	}

	void takeListSelectionChanged(String itemname) {
		GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);

		Item item = Items.findItemByName(itemname);
		GameWindow.panelRoom.setText(item.textOnPickup);

		Inventar.addToInv(item);
		currentRoom.removeItemFromRoom(item);
		
		switch (item.name) {
		case "Raumanzug":
			Omis.getOmiByName("Vogelscheuche").textOnObserve = "Das leere Gestell der Vogelscheuche, auf dem vorher der Raumanzug hing. Es quitscht noch immer.";
			break;
		case "Billardqueue":
			Omis.getOmiByName("Billardtisch").textOnObserve = "Ein ultramoderner Billardtisch in Wei� mit blauen Neonlichtern. Er ist leer.";
			break;
		}

		System.out.println("Add " + item.name + " to inventar");
	}

	void useListSelectionChanged(String name) {
		GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);

		BaseItem item = BaseItems.getByName(name);

		switch (name) {
		case "Kartenspiel":
			GameWindow.panelRoom.setText(item.textOnUse);
			DeadTimer.interval = DeadTimer.interval - (60 * 20);
			break;
		case "Board-Computer":
			GameWindow.currentWindow.switchPanel(MainPanels.Bordcomputer);
			break;
		case "Alien":
			GameWindow.currentWindow.switchPanel(MainPanels.AlienPanel);
			break;
		case "Automat":
			GameWindow.currentWindow.switchPanel(MainPanels.AutomatPanel);
			break;
		case "Roboter Klaus":
			GameWindow.currentWindow.switchPanel(MainPanels.KlausPanel);
			break;
		case "Verschlossene T�r zur Schleuse":
			GameWindow.currentWindow.switchPanel(MainPanels.PasswordPage);
			break;
		case "Schleusent�r":
			// Spielende
			// Pr�fe: Raumanzug nicht vorhanden
			if (!Inventar.isinInv("Geflickter Raumanzug") && !Inventar.isinInv("Raumanzug")) {
				GameWindow.currentWindow.switchScene(Scenes.GameOver7);
			}

			// Pr�fe: zu wenig Sauerstoff
			else if ((!Inventar.isinInv("Sauerstoffflasche A") && !Inventar.isinInv("Sauerstoffflasche B"))
					|| (!Inventar.isinInv("Sauerstoffflasche A") && !Inventar.isinInv("Jet-Pack"))
					|| (!Inventar.isinInv("Sauerstoffflasche B") && !Inventar.isinInv("Jet-Pack"))) {
				GameWindow.currentWindow.switchScene(Scenes.GameOver5);
			}

			// Pr�fe: Raumanzug kaputt
			else if (!Inventar.isinInv("Geflickter Raumanzug")) {
				GameWindow.currentWindow.switchScene(Scenes.GameOver6);
			}

			// Pr�fe: Schl�ssel vergessen
			else if (!Inventar.isinInv("Shuttle- Schl�ssel")) {
				GameWindow.currentWindow.switchScene(Scenes.GameOver4);
			}

			// Finale
			// Pr�fe: Virus vorhanden
			else if (GameLogic.currentGame.hasVirus) {
				GameWindow.currentWindow.switchScene(Scenes.GameOver3);
			} else {
				GameWindow.currentWindow.switchScene(Scenes.GameOver2);
			}
			break;
		default:
			GameWindow.panelRoom.setText(item.textOnUse);
			break;
		}
	}

	void combineListSelectionChanged(String itemname1, String itemname2) {
		GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);

		BaseItem item1 = BaseItems.getByName(itemname1);
		BaseItem item2 = BaseItems.getByName(itemname2);

		// find combinations
		if (!comibineHelper(item1, item2) & !comibineHelper(item2, item1)) {
			GameWindow.panelRoom.setText("Das geht nicht!");
		}

		System.out.println("comine " + itemname1 + " with " + itemname2);
	}

	private boolean comibineHelper(BaseItem item1, BaseItem item2) {
		int i = 0;
		for (BaseItem otherItem : item1.itemstocombinate) {
			if (otherItem.name.equals(item2.name)) {

				if (item1.itemdestruoncombinate.get(i)) {
					Inventar.removeFromInv(item1);
					Inventar.removeFromInv(item2);

					currentRoom.containedOmis.remove(item1);
					currentRoom.containedOmis.remove(item2);
				}

				BaseItem newItem = item1.itemscombinated.get(i);

				if (newItem == null) {

				} else if (newItem instanceof Item) {
					Inventar.addToInv(newItem);

					// Spezial-Fall: Taschenlampe
					if (newItem.name.equals("Geladene Taschenlampe")) {
						Rooms.getRoomByName("Area B").connections.remove(Rooms.getRoomByName("Maschinenraum "));
						Rooms.getRoomByName("Area B").connections.add(Rooms.getRoomByName("Maschinenraum"));
					}

					GameWindow.panelRoom.setText(((Item) newItem).textOnPickup);
				} else if (newItem instanceof Omi) {
					currentRoom.addOmi((Omi) newItem);
				}

				if (item1.connectToRoomOnCombinate.get(i) != null) {
					currentRoom.connections.add(item1.connectToRoomOnCombinate.get(i));
				}

				if (item1.textOnCombinate.get(i) != null) {
					GameWindow.panelRoom.setText(item1.textOnCombinate.get(i));
				}

				if (item1.execOnCombinate.get(i) != null) {
					System.out.println("test");
					item1.execOnCombinate.get(i).run();
				}
				return true;
			}

			i++;
		}
		return false;
	}
	// EndRegion

	// Region Button-Methoden
	void buttonPressedGoto() {
		System.out.println("Button pressed: Goto");
		if (currentRoom.countConnections() > 0) {
			GameWindow.currentWindow.switchPanel(MainPanels.GotoListPanel);
		} else {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			GameWindow.panelRoom.setText(
					"Du willst blind drauf los laufen, ohne Sinn und ohne Ziel. Du besinnst dich dann doch eines besseren und beschlie�t, dich erst einmal umzuschauen.");
		}
	}

	void buttonPressedObserve() {
		System.out.println("Button pressed: Observe");
		if ((currentRoom.countOmis() > 0 || Inventar.myInv.size() > 0 || currentRoom.countItems() > 0)
				&& currentRoom.observed) {
			GameWindow.currentWindow.switchPanel(MainPanels.ObserveListPanel);
		} else {
			currentRoom.observed = true;
			GameWindow.panelRoom.setText(currentRoom.textOnObserve);
			GameWindow.panelButtons.renameObserveButton("Ansehen");
		}
	}

	void buttonPressedTake() {
		System.out.println("Button pressed: Take");
		if (currentRoom.countItems() > 0 && currentRoom.observed) {
			GameWindow.currentWindow.switchPanel(MainPanels.TakeListPanel);
		} else {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			GameWindow.panelRoom.setText(
					"Dir ist danach, einfach mal irgendetwas aufzuheben und in deine Taschen zu stecken aber du wei�t einfach nicht was.");
		}
	}

	void buttonPressedUse() {
		System.out.println("Button pressed: Use");

		GameWindow.currentWindow.switchPanel(MainPanels.UseListPanel);
	}

	void buttonPressedUseWith() {
		System.out.println("Button pressed: UseWith");
		if ((currentRoom.countOmis() > 0 || Inventar.myInv.size() > 0) && currentRoom.observed) {
			GameWindow.currentWindow.switchPanel(MainPanels.UseCombinePanel);
		}
	}
	// EndRegion
}
