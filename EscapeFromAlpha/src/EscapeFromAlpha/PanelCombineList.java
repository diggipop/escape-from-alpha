package EscapeFromAlpha;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.ListSelectionModel;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;

public class PanelCombineList extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Image backgroundImage;
	private DefaultListModel<CustomListEntry> data = new DefaultListModel<CustomListEntry>();
	private JList<CustomListEntry> list1;
	private JList<CustomListEntry> list2;
	

	public void update() {
		backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
		data.removeAllElements();

		for (Omi omi : GameLogic.currentGame.currentRoom.containedOmis) {
			if (omi.observed) {
				data.addElement(new CustomListEntry(omi.name, false, omi.observed));
			}
		}

		for (BaseItem item : Inventar.myInv) {
			data.addElement(new CustomListEntry(item.name, true, item.observed));
		}

	}

	/**
	 * Create the panel.
	 */
	public PanelCombineList() {
		setBackground(Color.GREEN);
		setBorder(new EmptyBorder(50, 150, 50, 150));
		setLayout(new BorderLayout(0, 0));

		JTextPane header = new JTextPane();
		header.setEditable(false);
		header.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		header.setForeground(Color.WHITE);
		header.setFont(new Font("Arial", Font.PLAIN, 22));
		header.setBackground(new Color(0, 0, 0, .5f));
		header.setText("Was m�chtest du kombinieren? (W�hle 2 Gegenst�nde)");
		add(header, BorderLayout.NORTH);
		

		ListSelectionListener listener = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (list1.getSelectedIndex() >= 0 && list2.getSelectedIndex() >= 0 ) {
					GameLogic.currentGame.combineListSelectionChanged(
							((CustomListEntry)list1.getSelectedValue()).text,
							((CustomListEntry)list2.getSelectedValue()).text
						);
				}
			}
		};
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		add(splitPane, BorderLayout.CENTER);
		splitPane.setBackground(new Color(0,0,0,0));
		splitPane.setOpaque(true);
		
		list2 = new JList<CustomListEntry>(data);
		splitPane.setRightComponent(list2);
		list2.setAlignmentY(Component.TOP_ALIGNMENT);
		list2.setAlignmentX(Component.LEFT_ALIGNMENT);
		list2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list2.setFont(new Font("Arial", Font.PLAIN, 12));
		list2.setForeground(Color.WHITE);
		list2.setBackground(new Color(0, 0, 0, .5f));
		list2.setCellRenderer(new CustomCellRenderer());
		
		
		list1 = new JList<CustomListEntry>(data);
		splitPane.setLeftComponent(list1);
		list1.setAlignmentY(Component.TOP_ALIGNMENT);
		list1.setAlignmentX(Component.RIGHT_ALIGNMENT);
		list1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list1.setFont(new Font("Arial", Font.PLAIN, 12));
		list1.setForeground(Color.WHITE);
		list1.setBackground(new Color(0, 0, 0, .5f));
		list1.setCellRenderer(new CustomCellRenderer());
		list1.addListSelectionListener(listener);
		list2.addListSelectionListener(listener);
		
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.paintComponents(g);
	}
}