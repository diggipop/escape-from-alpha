package EscapeFromAlpha;

//java Program to create a dialog within a dialog 
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

class Klaus extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Image backgroundImage;

	String textb;
	String textc;
	String texte;
	String textf;

	JButton b;
	JButton c;
	JButton e;
	JButton h;
	JButton i;
	Insets insets;
	Dimension size;

	final JTextPane field;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);
		this.repaint();
	}

	// frame
	static JPanel f;
	private JLayeredPane layeredPane;
	Klaus() {
		this.setOpaque(false);
		setBackground(new Color(1, 0, 0, 0));
		setForeground(Color.white);
		setBorder(new EmptyBorder(50, 50, 50, 50));
		// create a new frame
		f = this;

		// create a object
		Klaus s = this;

		// Variablen f�r text
		textb = "Ich bin hier, um dich aufzumuntern.";
		textc = "Was machst du hier?";
		texte = "Ich h�tte gerne eine Sauerstoffflasche.";
		textf = "�Ich bin die Kompakte Lager und Sicherungseinheit, hier um zu helfen und zu dienen.� "
				+ "Er macht eine Pause und spricht dann weiter: �Warum st�rst du mich beim Sterben?�";

		// panel im panel
		Font font = new Font("Arial", Font.PLAIN, 22);
		setLayout(new BorderLayout(0, 0));
		
		JPanel inneres = new JPanel();
		inneres.setVisible(true);
		
		field = new JTextPane();
		field.setBackground(new Color(0, 0, 0, 0.8f));
		field.setFont(font);
		field.setForeground(Color.white);
		field.setEditable(false);
		field.setText(textf);
		//field.setLineWrap(true);
		//field.setWrapStyleWord(true);
		
		StyledDocument doc = field.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		
		
		add(field, BorderLayout.CENTER);

		layeredPane = new JLayeredPane();
		layeredPane.setLayout(new BoxLayout(layeredPane, BoxLayout.Y_AXIS));
		add(layeredPane, BorderLayout.SOUTH);
		
		b = new JButton(textb);
		b.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(b);
		c = new JButton(textc);
		c.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(c);
		e = new JButton(texte);
		e.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(e);
		h = new JButton("");
		h.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(h);
		i = new JButton("");
		i.setAlignmentX(Component.CENTER_ALIGNMENT);
		layeredPane.add(i);
		
		i.addActionListener(s);
		i.setVisible(false);
		h.addActionListener(s);
		h.setVisible(false);
		e.addActionListener(s);
		c.addActionListener(s);
		b.addActionListener(s);
			
		b.setBackground(new Color(0,0,0,1f));
		b.setForeground(Color.white);

		c.setBackground(new Color(0,0,0,1f));
		c.setForeground(Color.white);

		e.setBackground(new Color(0,0,0,1f));
		e.setForeground(Color.white);

		h.setBackground(new Color(0,0,0,1f));
		h.setForeground(Color.white);
		
		i.setBackground(new Color(0,0,0,1f));
		i.setForeground(Color.white);		
		
		insets = b.getInsets();
		
		if (GameLogic.currentGame.currentRoom != null)
			backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
		
		f.setVisible(true);
	}

	public void actionPerformed(ActionEvent event) {
		String s = event.getActionCommand();
		
		if (s.equals("Ich bin hier, um dich aufzumuntern.")) {
			field.setText("Das schafft keiner!");
			b.setText("Ich bin hier, um dich aufzumuntern.");
			c.setText("Was machst du hier?");
			e.setText("Ich h�tte gerne eine Sauerstoffflasche.");
		}

		else if (s.equals("Was machst du hier?")) {
			field.setText("Ich warte auf den Tod.");
			b.setText("Ich bin hier, um dich aufzumuntern.");
			c.setText("Was machst du hier?");
			e.setText("Ich h�tte gerne eine Sauerstoffflasche.");

		} else if (s.equals("Ich h�tte gerne eine Sauerstoffflasche.")) {
			field.setText("Ach ja? Wozu?");
			b.setText("Ich will weg von dieser Raumstation");
			c.setText("Ich will sie einem Alien �ber den Kopf ziehen");
			e.setText("Ich will sie zur Explosion bringen, um das alles hier zu beenden.");

		} else if (s.equals("Ich will weg von dieser Raumstation")) {
			field.setText("Das hat doch keinen Sinn.");
			b.setText("Ich will weg von dieser Raumstation");
			c.setText("Ich will sie einem Alien �ber den Kopf ziehen");
			e.setText("Ich will sie zur Explosion bringen, um das alles hier zu beenden.");

		} else if (s.equals("Ich will sie einem Alien �ber den Kopf ziehen")) {
			field.setText("Das schaffst du doch eh nicht.");
			b.setText("Ich will weg von dieser Raumstation");
			c.setText("Ich will sie einem Alien �ber den Kopf ziehen");
			e.setText("Ich will sie zur Explosion bringen, um das alles hier zu beenden.");

		} else if (s.equals("Ich will sie zur Explosion bringen, um das alles hier zu beenden.")) {
			field.setText("Das kann ich nachvollziehen. Aber warum sollte ich sie dir geben?");
			b.setText("Weil es dein Job ist!");
			c.setText("Weil ich dich nett darum bitte?");
			e.setText("Damit du sie nicht mehr bewachen musst.");

		} else if (s.equals("Weil es dein Job ist!")) {
			field.setText("Der Roboter n�rgelt ein wenig vor sich hin und gibt dir dann die Flasche. "
					+ "�Ich hasse dich und will dich nie wieder sehen!�");
			b.setText("Klaus in Ruhe lassen.");

			GameLogic.currentGame.currentRoom.containedOmis.remove(Omis.getOmiByName("Roboter Klaus"));
			if(! Inventar.isinInv("Sauerstoffflasche B")) {
				Inventar.addToInv(Items.findItemByName("Sauerstoffflasche B"));
			}
				
			c.setVisible(false);
			e.setVisible(false);

		} else if (s.equals("Klaus in Ruhe lassen.")) {
			GameWindow.currentWindow.switchPanel(MainPanels.RoomPanel);
			GameWindow.currentWindow.panelRoom.setText( GameLogic.currentGame.currentRoom.textOnObserve );

		} else if (s.equals("Weil ich dich nett darum bitte?")) {
			field.setText("Nett hat noch keinem geholfen.");
			b.setText("Weil es dein Job ist!");
			c.setText("Weil ich dich nett darum bitte?");
			e.setText("Damit du sie nicht mehr bewachen musst.");

		} else if (s.equals("Damit du sie nicht mehr bewachen musst.")) {
			field.setText("Das macht mir nichts aus.");
			b.setText("Weil es dein Job ist!");
			c.setText("Weil ich dich nett darum bitte?");
			e.setText("Damit du sie nicht mehr bewachen musst.");
		}
	}
}