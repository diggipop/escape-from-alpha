package EscapeFromAlpha;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
 

public class ButtonListener extends JPanel implements ActionListener
{
    
	private static final long serialVersionUID = 1L;
	JButton button1;
    JButton button2;
    JButton button3;
    JButton button4;
    JButton button5;
    
    JLabel label;
    JPanel panel;
 
    public void renameObserveButton(String name) {
    	button2.setText(name);
    }
    
    public ButtonListener(){
        //this.setTitle("ActionListener Beispiel");
        this.setSize(400, 200);
        setBackground(Color.black);
        panel = new JPanel();
        panel.setBackground(Color.black);
        
        // Leeres JLabel-Objekt wird erzeugt
        label = new JLabel();
        label.setBackground(Color.black);
 
        //Buttons werden erstellt
        button1 = new JButton("Gehe zu");
        button2 = new JButton ("Erkunde");
        button3 = new JButton ("Nimm");
        button4 = new JButton ("Benutze / Sprich mit");
        button5 = new JButton ("Benutze mit");
        
        button1.setBackground(new Color(0,0,0,1f));
        button1.setForeground(Color.white);

        button2.setBackground(new Color(0,0,0,1f));
        button2.setForeground(Color.white);

        button3.setBackground(new Color(0,0,0,1f));
        button3.setForeground(Color.white);

        button4.setBackground(new Color(0,0,0,1f));
        button4.setForeground(Color.white);
		
        button5.setBackground(new Color(0,0,0,1f));
        button5.setForeground(Color.white);	
 
        //Buttons werden dem Listener zugeordnet
        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        button4.addActionListener(this);
        button5.addActionListener(this);
        
 
        //Buttons werden dem JPanel hinzugef�gt
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
 
 
        //JLabel wird dem Panel hinzugef�gt
        panel.add(label);
        this.add(panel);
    }
 
    public void actionPerformed (ActionEvent ae){
        // Die Quelle wird mit getSource() abgefragt und mit den
        // Buttons abgeglichen. Wenn die Quelle des ActionEvents einer
        // der Buttons ist, wird der Text des JLabels entsprechend ge�ndert
        if(ae.getSource() == this.button1){
            GameLogic.currentGame.buttonPressedGoto();
        }
        else if(ae.getSource() == this.button2){
        	GameLogic.currentGame.buttonPressedObserve();
        }
        else if (ae.getSource() == this.button3){
        	GameLogic.currentGame.buttonPressedTake();
        }
        
        else if (ae.getSource() == this.button4){
        	GameLogic.currentGame.buttonPressedUse();
        }
        else if (ae.getSource() == this.button5){
        	GameLogic.currentGame.buttonPressedUseWith();
        }
    }
}