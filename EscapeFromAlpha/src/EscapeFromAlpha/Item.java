package EscapeFromAlpha;

public class Item extends BaseItem {
	String textOnPickup;
	boolean removeOnUse;
	public Boolean observed = false;

	public Item(String name, String textOnObserve, String textOnUse, String textOnPickup, boolean removeOnUse) {
		super(name,textOnObserve,textOnUse);
		this.textOnPickup = textOnPickup;
		this.removeOnUse = removeOnUse;
	}
}
