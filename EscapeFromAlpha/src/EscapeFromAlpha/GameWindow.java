package EscapeFromAlpha;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javazoom.jl.decoder.JavaLayerException;

// Color
import java.awt.Color;

enum MainPanels {
	RoomPanel, GotoListPanel, ObserveListPanel, TakeListPanel, UseListPanel, UseCombinePanel, PasswordPage, AlienPanel,
	KlausPanel, Bordcomputer, AutomatPanel
};

enum Scenes {
	Title, Game, GameOver1, GameOver2, GameOver3, GameOver4, GameOver5, GameOver6, GameOver7, GameOverAlien1,
	GameOverAlien2, Credits
};

public class GameWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	public static GameWindow currentWindow;
	public static JPanel gamePanel;
	public static JPanel mainPanel = null;
	public static ButtonListener panelButtons;

	public static JPanel passwordPage;
	public static PanelRoom panelRoom;
	public static PanelGotoList panelGotoList;
	public static PanelObserveList panelObserveList;
	public static PanelTakeList panelTakeList;
	public static PanelUseList panelUseList;
	public static PanelCombineList panelCombineList;

	// game end
	public static TitleScene title;
	public static EndScene end1, end2, end3, end4, end5, end6, end7, endAlien1, endAlien2;
	public static Credits credits;

	public static Music music;
	
	/**
	 * Create the frame.
	 */
	public GameWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 300, 800, 600);
		gamePanel = new JPanel();
		gamePanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		gamePanel.setLayout(new BorderLayout(0, 0));

		title = new TitleScene( GameWindow.class.getResource("Start.png") );
		 
		setContentPane(title);

		panelButtons = new ButtonListener();
		gamePanel.add(panelButtons, BorderLayout.SOUTH);

		panelRoom = new PanelRoom();
		panelGotoList = new PanelGotoList();
		panelObserveList = new PanelObserveList();
		panelTakeList = new PanelTakeList();
		panelUseList = new PanelUseList();
		panelCombineList = new PanelCombineList();

		end1 = new EndScene("Game Over", 30, "Du hast es nicht geschafft, die Station rechtzeitig zu verlassen.  Dir f�llt es schon seit einiger Zeit schwererer zu atmen. Dir ist schwindelig und du f�hlst dich benommen. Der Bordcomputer teilt dir mit, dass nun s�mtliche Sauerstoffreserven aufgebraucht sind, was du nur noch am Rande deiner Wahrnehmung h�rst. Du erstickst und stirbst.", 22, Color.WHITE, new Color(0, 0, 0, .8f),
				"Arial", "EndDead.png");
		
		end2 = new EndScene("Geschafft!", 30, "Du hast es sicher und mit gen�gend Sauerstoff zum Shuttle geschafft, der rote Hering und nicht etwa der Schl�ssel �ffnet das Raumschiff. Alle Systeme sind wie durch ein Wunder bereit. Du startest das Raumschiff und es hebt ab. Nachdem du den Kurs zur Erde programmiert hast, siehst du aus dem Fenster zur�ck auf den Mond und erkennst, wie eine schwarze, wimmelnde Masse sich auf die Station zubewegt. Nach einer Woche Flug landest du sicher auf der Erde. Du hast es geschafft. Gut gemacht.", 22, Color.WHITE,
				new Color(0, 0, 0, .8f), "Arial", "EndEscape1.png");
		
		end3 = new EndScene("Geschafft!", 30, "Du hast es sicher und mit gen�gend Sauerstoff zum Shuttle geschafft, der rote Hering und nicht etwa der Schl�ssel �ffnet das Raumschiff. W�hrend deines Spaziergangs konntest du Aliens erkennen, die dich aus einiger Entfernung beobachtet haben, aber nichts unternahmen um dich zu stoppen. Du startest das Raumschiff und es hebt ab. Nachdem du den Kurs zur Erde programmiert hast, siehst du aus dem Fenster zur�ck auf den Mond und erkennst, wie eine schwarze, wimmelnde Masse sich um die Startplattform versammelt hat. Nach einer Woche Flug landest du sicher auf der Erde. Du hast einen unbekannten Virus mit auf die Erde gebracht. Es gibt keine Heilung f�r dich. Nach knapp drei Monaten stirbst du und kurz nach dir die H�lfte der Menschheit. Thanos w�re stolz auf dich.", 22, Color.WHITE,
				new Color(0, 0, 0, .8f), "Arial", "EndEscape2.png");
		
		end4 = new EndScene("Game Over", 30, "Du hast es sicher und mit gen�gend Sauerstoff zum Shuttle geschafft, als du das Raumschiff �ffnen willst, f�llt dir voller Entsetzen auf, dass du gar keinen Schl�ssel daf�r hast. Deine Sauerstoffreserven reichen nicht, um zur Station zur�ck zu kehren. Du setzt dich neben das Shuttle auf den Boden, und erstickst kurz vor dem Ziel. Du bist gestorben.", 22, Color.WHITE,
				new Color(0, 0, 0, .8f), "Arial", "EndDead.png");
		
		end5 = new EndScene("Game Over", 30, "Du trittst aus der Schleuse und begibst dich auf den langen Weg zum Spaceshuttle, welches du in weiter Ferne aufragen siehst. Niemand verfolgt dich und du f�hlst Erleichterung. Auf der H�lfte der Strecke geht dir allerdings der Sauerstoff aus und du erstickst auf dem R�cken liegen, den Blick Richtung Sterne gerichtet. Du bist gestorben.", 22,
				Color.WHITE, new Color(0, 0, 0, .8f), "Arial", "EndDead.png");
		
		end6 = new EndScene("Game Over", 30, "Du trittst aus der Schleuse, gehst ein paar Schritte und stirbst. Dein Anzug war undicht und die nicht vorhandene Atmosph�re auf dem Mond gepaart mit der K�lte des Weltalls l�sst dir sprichw�rtlich das Blut in den Adern gefrieren. Du bist gestorben.", 22, Color.WHITE,
				new Color(0, 0, 0, .8f), "Arial", "EndDead.png");
		
		end7 = new EndScene("Game Over", 30, "Du �ffnest die Schleuse und stirbst nahezu augenblicklich. Was hast du dir nur dabei gedacht, ohne Raumanzug aus der Schleuse zu treten? Du bist gestorben.", 22,
				Color.WHITE, new Color(0, 0, 0, .8f), "Arial", "EndDead.png");
		
		endAlien1 = new EndScene("Game Over", 30, "Das Alien zerfleischt dich und du bist tot.", 22, Color.WHITE,
				new Color(0, 0, 0, .8f), "Arial", "EndDead.png");
		
		endAlien2 = new EndScene("Game Over", 30,
				"<html>Ehe du weitersprechen kannst, hat dir das Alien den Kopf abgerissen. <br> Du bist auf der Stelle tot.<html>",
				22, Color.WHITE, new Color(0, 0, 0, .8f), "Arial", "EndDead.png");

		credits = new Credits();

		passwordPage = new CheckPassword();

		switchPanel(MainPanels.RoomPanel);
		currentWindow = this;
	}

	public void switchScene(Scenes scene) {
		switch (scene) {
		case Title:
			setContentPane(title);
			break;
		case Game:
			setContentPane(gamePanel);
			break;
		case GameOver1:
			setContentPane(end1);
			break;
		case GameOver2:
			setContentPane(end2);
			break;
		case GameOver3:
			setContentPane(end3);
			break;
		case GameOver4:
			setContentPane(end4);
			break;
		case GameOver5:
			setContentPane(end5);
			break;
		case GameOver6:
			setContentPane(end6);
			break;
		case GameOver7:
			setContentPane(end7);
			break;
		case GameOverAlien1:
			setContentPane(endAlien1);
			break;
		case GameOverAlien2:
			setContentPane(endAlien2);
			break;
		case Credits:
			setContentPane(credits);
			Credits.Start(this);
			break;
		}
	}

	public void switchPanel(MainPanels panel) {
		if (mainPanel != null)
			gamePanel.remove(mainPanel);

		switch (panel) {
		case RoomPanel:
			mainPanel = panelRoom;
			panelRoom.update();
			break;
		case GotoListPanel:
			mainPanel = panelGotoList;
			panelGotoList.update();
			break;
		case ObserveListPanel:
			mainPanel = panelObserveList;
			panelObserveList.update();
			break;
		case TakeListPanel:
			mainPanel = panelTakeList;
			panelTakeList.update();
			break;
		case UseListPanel:
			mainPanel = panelUseList;
			panelUseList.update();
			break;
		case UseCombinePanel:
			mainPanel = panelCombineList;
			panelCombineList.update();
			break;
		case PasswordPage:
			mainPanel = passwordPage;
			break;
		case AutomatPanel:
			mainPanel = new DialogAutomat();
			break;
		case AlienPanel:
			mainPanel = new Alien();
			break;
		case KlausPanel:
			mainPanel = new Klaus();
			break;
		case Bordcomputer:
			mainPanel = new BordComputer();
			break;
		}

		gamePanel.add(mainPanel, BorderLayout.CENTER);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		this.paintComponents(g);
		this.repaint();
	}

	public static void main(String[] args) {
		music = new Music();
		try {
			music.setRepeat(true);
			music.play("atmo.mp3");

		} catch (JavaLayerException | IOException | URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new GameLogic();
					GameWindow window = new GameWindow();
					window.setVisible(true);

					//game.startGame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
