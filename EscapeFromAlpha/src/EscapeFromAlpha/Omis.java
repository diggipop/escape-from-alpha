package EscapeFromAlpha;

import java.util.ArrayList;

public class Omis {
	public static ArrayList<Omi> omis = new ArrayList<Omi>();

	public static Omi getOmiByName(String name) {
		for (Omi omi : omis) {
			if (omi.name.equals(name)) {
				return omi;
			}
		}
		System.out.println("ERROR: getOmiByName could not find " + name);
		return null;
	}

	public Omis() {

		Omi tablesforPlants = new Omi("Anzuchttische f�r Pflanzen",
				"Gro�e niedrige Tische mit Pflanzen in verschiedenen Wachstumsphasen.",
				"Du kannst damit nichts anfangen.", false);

		Omi automatklaus = new Omi("Roboter Klaus",
				"Vor dir steht ein zwei Meter gro�er wei�er humanoider Roboter mit einem zu gro�en Kopf und rot-leuchtenden dreieckigen Augen. "
						+ "Er hat eine blaue, f�r seinen Kopf zu kleine M�tze mit der Aufschrift KLAUS auf. Klaus wirkt irgendwie deprimiert. ",
				"?", false);
		automatklaus.showForUse = true;

		Omi bed = new Omi("Bett", "Ein spartanisches aber gem�tliches Bett mit einem gro�en Kissen.",
				"Du schaust unter das Kissen und findest dort ein Feuerzeug.", false);
		bed.containedItems.add(Items.findItemByName("Feuerzeug"));

		Omi billiardTable = new Omi("Billardtisch",
				"Ein ultramoderner Billardtisch in Wei� mit blauen Neonlichtern. Die Kugeln sind verschwunden, aber es ist noch ein Queue vorhanden.",
				"Du nimmst den Queue mit und f�hlst dich ziemlich cool dabei.", false);
		billiardTable.containedItems.add(Items.findItemByName("Billardqueue"));

		Omi floor = new Omi("Boden",
				"Der Boden war urspr�nglich mal sch�n wei� und gl�nzend, jetzt ist er sch�n blutig und verkrustet. In einer Ecke entdeckst du eine Energiezelle, die leicht leuchtet.",
				"Du siehst hier nichts, was du dir l�nger anschauen m�chtest.", false);
		floor.containedItems.add(Items.findItemByName("Energiezelle"));

		Omi boxingRing = new Omi("Boxring",
				"Ein gro�er Boxring, der von wei�en Seilen umrandet wird. Im Boxring liegen ein totes Alien und ein toter Mensch, beide haben Boxhandschuhe an.",
				"?", false);

		Omi pcScreens = new Omi("Computerbildschirme",
				"Die Bildschirme flackern, du siehst, wie zerst�rt die Station von au�en wirklich aussieht. Auf Kamerabildern wird gezeigt, wie Sauerstoff austritt.",
				"?", false);

		Omi generator = new Omi("Generator",
				"Der Generator, der die Station mit Strom versorgt. Er ist intakt und surrt leise vor sich hin. Du verstehst nichts von Maschinen und l�sst ihn in Ruhe.",
				"?", false);

		Omi GeorgWBush = new Omi("Georg W. Bush",
				"Ein gro�er k�nstlicher Busch mit 2 kleinen Augen und einem Mund. Also du auf ihn zugehst, stellt er sich als Georg W. vor, wackelt hin und her und versucht dir ein Gespr�ch ans Bein zu binden. Also du entsetzt einen Schritt zur Seite machst, entdeckst du hinter dem Busch einen gro�en L�ftungsschacht.",
				"?", false);

		Omi towelDispenser = new Omi("Handtuchspender",
				"Ein edler Handtuchspender. Er spendet Handt�cher und sieht edel dabei aus.",
				"Du nimmst ein Tuch mit. Scheinbar bist du ein Messi.", false);
		towelDispenser.containedItems.add(Items.findItemByName("T�cher"));

		Omi bench = new Omi("Hantelbank",
				"Eine stabile Hantelbank. An den Kurzhanteln klebt eine seltsame Fl�ssigkeit."
						+ " Neben der Hantelbank entdeckst du eine Key-Card."
						+ " Du entscheidest, dass du dich heute stark genug f�hlst und l�sst die Hanteln in Ruhe.",
				"?", false);
		bench.containedItems.add(Items.findItemByName("Key-Card:Lagerraum"));

		Omi alien = new Omi("Alien",
				"Du siehst eine T�r. �ber der T�r steht Kommandozentrale. Vor der T�r steht ein Alien. Es grinst dich vielzahnig an.",
				"?", false);
		alien.showForUse = true;

		Omi kitchenCabinet = new Omi("K�chenschrank",
				"Du siehst eine K�chenzeile mit vielen offenen Schubladen. Auf der K�chenzeile liegt gammeliges Obst und offene leere Nahrungsverpackungen.",
				"Du nimmst ein Messer aus einer der Schubladen, warum auch nicht?", false);
		kitchenCabinet.containedItems.add(Items.findItemByName("Messer"));

		Omi fridge = new Omi("K�hlschrank",
				"Ein gro�er wei�er K�hlschrank mit einer silbernen Herstellerplakette auf der �KIRK� steht. Der K�hlschrank steht leicht schr�g im Raum. Du schaust hinter den K�hlschrank und siehst dort neben vielen Flusen auch ein leuchtendes Objekt.",
				"Mit dem Besen fischst du das flache Objekt hinter dem K�hlschrank hervor. Es ist ein Tablet, du steckst es ein.",
				false);

		Omi storageRake = new Omi("Lagerregal",
				"Vor dir siehst du ein riesiges Lagerregal, was sich bis zur Decke des Raumes erstreckt. Du siehst unz�hlige Kartons und Packungen, hast aber weder Lust noch Zeit sie alle zu durchsuchen. Allerdings f�llt dir eine Flasche Rum ins Auge, die auf dich sehr ansprechend wirkt.",
				"Du nimmst den Rum an dich. Im Moment ist er dein einziger Freund.", false);
		storageRake.containedItems.add(Items.findItemByName("Flasche Top-Rum"));

		Omi funnel = new Omi("L�ftungsschacht",
				"Ein erstaunlich gro�er und vergitterter L�ftungsschacht. In seinem Inneren zuckt ein gro�er Ventilator, in dessen Bl�ttern sich eine blutige Masse verfangen hat, weswegen er sich nicht weiterdreht. Zwischen dem Gitter und der Wand befindet sich ein 3-Finger breiter Spalt. Als du am Gitter ziehst, gibt es ein wenig nach aber deine Kraft reicht nicht, um es zu �ffnen",
				"?", false);

		Omi neighboringCabin = new Omi("Nachbarkabine",
				"Neugierig �ffnest du die Nachbarkabine. Ein �bler Geruch str�mt dir entgegen, �berall klebt � Zeug. In der Wand steckt eine Axt, die eine krallenbew�hrte Hand an die Wand nagelt.  ",
				"Du zerrst die Axt aus der Wand, schleuderst angewidert die Hand von dir und hast nun eine Axt an deiner Seite. Es geht dir gleich viel besser.",
				false);
		neighboringCabin.containedItems.add(Items.findItemByName("Axt"));

		Omi cleaningCart = new Omi("Putzwagen",
				"In einer Ecke findest du einen kleinen verlassenen Putzwagen. Du fragst dich nach dem Zweck dieses nahezu historischen Objektes, da du auf deinem Weg schon mehrere Putzroboter gesehen hast, die verzweifelt versuchten das Chaos zu beseitigen. An dem Putzwagen befindet sich als einziges intaktes Objekt lediglich ein einzelner Besen.",
				"Du h�ltst den Dreck nicht mehr aus und nimmst den Besen an dich. Jetzt wirst du hier mal aufr�umen!",
				false);
		cleaningCart.containedItems.add(Items.findItemByName("Besen"));

		Omi robot = new Omi("Automat",
				"Du siehst einen prototypischen Automaten, der aussieht wie alle anderen Automaten, die man so kennt.",
				"?", false);
		robot.showForUse = true;

		Omi sandbag = new Omi("Sandsack",
				"Mitten im Raum h�ngt ein gro�er schwarzer Sandsack. Er wurde offensichtlich aufgeritzt, denn es liegt ein Sandhaufen unter ihm. Als du um ihn herumgehst, siehst du, dass er nun mit den �berresten von Menschen gef�llt ist. Der Sack h�ngt an einem langen Seil, welches �ber Wand und Decke befestigt ist. Es k�nnte n�tzlich sein.",
				"Mit viel Muskelkraft und dem Messer schneidest du den Sandsack los und sammelst das Seil ein.", false);

		Omi oxygenTank = new Omi("Sauerstofftank",
				"Der Sauerstofftank, der die gesamte Station versorgt. Er nimmt die gesamte hintere Wand ein. Sein Kontrolldisplay zeigt, dass ein irreparabler Schaden vorliegt und der restliche Sauerstoff nur noch f�r kurze Zeit reicht.",
				"?", false);

		Omi flootDoor = new Omi("Schleusent�r",
				"Die Schleusent�r, die auf die Mondoberfl�che f�hrt. Jeder Sicherheitsmechanismus wurde entfernt und du kannst sie einfach �ffnen, wenn du willst. Durch ihr Bullauge siehst du die wei�-leuchtende Oberfl�che. ",
				"?", false);
		flootDoor.showForUse = true;

		Omi heavyBox = new Omi("Schwere Kiste",
				"Du siehst eine gro�e Kiste, unter der ein Feuerl�scher klemmt. Als du versuchst die Kiste zu bewegen, r�hrt sie sich kaum. Auch nach mehreren Versuchen bekommst du den Feuerl�scher nicht frei. Aber du willst ihn unbedingt haben.",
				"Du bindest das Seil um die Kiste, ziehst so lange, bis sie ein wenig in deine Richtung kippt und bindest das Seil dann an einem Lufthaken fest. Jetzt kannst du den Feuerl�scher nehmen. Direkt nachdem du den L�scher herausgezogen hast, rei�t das Seil und die Kiste kracht zur�ck auf den Boden. Du erschreckst dich f�rchterlich, aber das war es wert.",
				false);

		Omi sideBoard = new Omi("Sideboard",
				"Das einzig interessante an diesem Sideboard (fr�her auch einfach niedriger Schrank genannt)  ist ein Radio, welches darauf steht.",
				"Du nimmst das Radio an dich, denn du willst nicht l�nger mit deinen Gedanken und dem Bordcomputer alleine sein.",
				false);
		sideBoard.containedItems.add(Items.findItemByName("entladenes Radio"));
		
		Omi sofa = new Omi("Sofa",
				"Ein ehemals einladendes Sofa, mit ehemals weichem Polster und ehemals wei�en Kissen. Au�er der eklatanten Ver�nderung in Farbe, Struktur und Gem�tlichkeit, ist nichts interessant an dem Sofa.",
				"?", false);

		Omi locker = new Omi("Spint",
				"Du siehst eine Reihe offenstehender Spinte, in denen die ehemaligen Bewohner ihren Krimskrams wegschlie�en konnten, damit die Putzroboter sie nicht wegr�umen. In einem der Spinte findest du eine Taschenlampe.",
				"Du nimmst die Taschenlampe an dich, denn es kann viel passieren.", false);
		locker.containedItems.add(Items.findItemByName("Entladene Taschenlampe"));

		Omi flushing = new Omi("Sp�lung",
				"Du bet�tigst die Klosp�lung. Sie funktioniert einwandfrei und saugt alles ab, was sich in der Klosch�ssel befand. Sauber!",
				"?", false);

		Omi controlPanel = new Omi("Steuerpult",
				"Du stehst vor dem halbzerst�rten Steuerpult der Station. Bunte Lichter, die dir nichts sagen und Anzeigen, die du nicht verstehst leuchten vor dir auf. Als du verzweifelt auf deine Knie f�llst, entdeckst du einen Schl�sselbund, welcher unter dem Pult liegt.",
				"Du steckst den Schl�sselbund ein.", false);
		controlPanel.containedItems.add(Items.findItemByName("Shuttle- Schl�ssel"));

		Omi bar = new Omi("Theke",
				"Eine schicke blutrote Bartheke mit schicken umgeworfenen Barhockern, schicken zerbrochenen Gl�sern und ganz ohne intakte Flaschen. Bitte geh weiter, es gibt hier nichts zu sehen.",
				"?", false);

		Omi table = new Omi("Tisch",
				"Ein runder Tisch, �berzogen mit gr�nem Filz, welcher wiederum mit braunen Flecken �berzogen ist. Auf dem Tisch liegen diverse Essensmarken, und andere als Spielchips verwendete Objekte, sowie ein Satz Karten.",
				"PLACEHOLDER: UseText",
				false);
		table.containedItems.add(Items.findItemByName("Kartenspiel"));

		Omi tableswithDrawer = new Omi("Tische mit Schublade",
				"Du siehst einen kleinen unscheinbaren Beistelltisch. Auf dem Tisch befindet sich nichts aber er hat eine Schublade.",
				"PLACEHOLDER: Betrachte Schublade", false);
		
		Omi drawer = new Omi("Schublade",
				"Du siehst eine ganz normale geschlossene Schublade. Du kannst dich vor Spannung, was darin wohl sein k�nnte, kaum noch zur�ckhalten, sie zu �ffnen.",
				"Du ziehst an der Schublade, sie �ffnet sich ohne Widerstand. In ihr entdeckst du eine Bibel. Du l�sst sie liegen.",
				false);
		drawer.showForUse = true;

		Omi lockedDoor1 = new Omi("Verschlossene T�r zu Area B",
				"Die Schleuse zum hinteren Teil des Flures steht einen Spalt offen und spr�ht Funken, sie ist zu stark besch�digt um sie zu �ffnen.",
				"?", false);

		Omi lockedDoor2 = new Omi("Verschlossene T�r des Lagerraumes",
				"Die Lagerraumt�r leuchtet rot. Du ben�tigst eine elektronische Key-Card um die T�r zu �ffnen. ",
				"Du steckst die Key-Card in den entsprechenden Slot, die T�r leuchtet hellblau auf und �ffnet sich mit einem wohligen Seufzen.",
				false);

		Omi lockedDoor3 = new Omi("Verschlossene T�r zur Schleuse",
				"Die T�r zur Schleuse leuchtet rot. Neben der T�r befindet sich ein Display, in das du ein vierstelliges Passwort eingeben musst.",
				"?", false);
		lockedDoor3.showForUse = true;

		Omi scarecrow = new Omi("Vogelscheuche",
				"Du siehst eine Art Vogelscheuche, zwischen den Anzuchttischen. Da es hier keinerlei V�gel gibt oder gab, ist sie wohl er scherzhaft als funktional. Sie besteht aus einem leeren alten Raumanzug, der �ber ein Gestell gezogen wurde. Das Gestell bewegt sich leicht und quietscht dabei unheimlich.",
				"Du l�st den Raumanzug vom Gestell und ziehst ihn direkt an. Nat�rlich passt er dir perfekt.", false);
		scarecrow.containedItems.add(Items.findItemByName("Raumanzug"));

		Omi wallHolder = new Omi("Wandhalterung Jet-Pack",
				"An einer Wand zwischen zwei Fenstern entdeckst du eine Wandhalterung an der ein Jet-Pack befestigt wurde. Darunter prangt eine kleine silberne Tafel auf der �Funktionelles Museumsst�ck� steht. ",
				"Gierig nimmst du das Jet-Pack an dich und schnallst es dir um. Du solltest es nur au�erhalb der Station verwenden.",
				false);
		wallHolder.containedItems.add(Items.findItemByName("Jet-Pack"));

		Omi warningSign = new Omi("Warntafel mit Checkliste",
				"Neben der T�r, die auf die Mondoberfl�che f�hrt h�ngt eine �bersichtsgrafik. Auf ihr steht:\r\n"
						+ "�Bitte denken Sie daran, dass mindestens 2 volle Sauerstoffflaschen ben�tigt werden, um zu Fu� zur Shuttle-Rampe zu gelangen.� Darunter klebt ein kleiner Notizzettel auf dem folgendes steht: �Hab die letzte Flasche genommen. Vielleicht hat KLAUS im Lager noch welche. Sry.� \r\n",
				"?", false);

		Omi boardComputer = new Omi("Board-Computer", "?", "?", false);
		Omi aliendead= new Omi("Brennendes Alien", 
				"Nachdem du das Alien mit dem Molotov beworfen hast, brennt es vor sich hin. Wegen dem Feuer kommst du noch immer nicht in die Kommandozentrale.","",false);
		
		Omi toolkitcase= new Omi("Werkzeugkoffer","Ein bereits gepl�nderter oranger Werkzeugkoffer mit der Aufschrift: NASA",
				"In seinem innern ist nichts mehr zu finden, aber unter seinem Deckel klebt noch eine Rolle Klebeband",
				false);
		toolkitcase.containedItems.add(Items.findItemByName("Klebeband"));
		
		Omi floor2 = new Omi("Flur-Boden",
				"Der Flurboden ist �bers�t mit Unrat, Blutspritzern und angetrockneter gr�ner Fl�ssigkeit. Neben einem Schachtgitter erkennst du ein leuchtendes Tablet.",
				"Du siehst hier nichts, was du dir l�nger anschauen m�chtest.", false);
		floor2.containedItems.add(Items.findItemByName("Logbuch 1"));
		
		Omi floor3 = new Omi("Garten-Boden",
				"Auf dem Boden liegen hier und da ein paar Bl�tter. Im hohen Gras leuchtet etwas.",
				"Du siehst hier nichts, was du dir l�nger anschauen m�chtest.", false);
		floor3.containedItems.add(Items.findItemByName("Logbuch 2"));
		
		Omi floor4 = new Omi("Maschinenraum-Boden",
				"Auf dem mit Riffelblech ausgelegten Boden liegen Tr�mmerteile des Sauerstofftanks und andere Maschinenteile. Unter den Teilen siehst du ein schwaches Leuchten.",
				"Du siehst hier nichts, was du dir l�nger anschauen m�chtest.", false);	
		floor4.containedItems.add(Items.findItemByName("Logbuch 3"));
		

		omis.add(tablesforPlants); // 0
		omis.add(automatklaus); // 1
		omis.add(bed); // 2
		omis.add(billiardTable); // 3
		omis.add(floor); // 4
		omis.add(boxingRing); // 5
		omis.add(pcScreens); // 6
		omis.add(generator); // 7
		omis.add(GeorgWBush); // 8
		omis.add(towelDispenser); // 9
		omis.add(bench); // 10
		omis.add(alien); // 11
		omis.add(kitchenCabinet); // 12
		omis.add(fridge); // 13
		omis.add(storageRake); // 14
		omis.add(funnel); // 15
		omis.add(neighboringCabin); // 16
		omis.add(cleaningCart); // 17
		omis.add(robot); // 18
		omis.add(sandbag); // 19
		omis.add(oxygenTank); // 20
		omis.add(flootDoor); // 21
		omis.add(heavyBox); // 22
		omis.add(sideBoard); // 23
		omis.add(sofa); // 24
		omis.add(locker); // 25
		omis.add(flushing); // 26
		omis.add(controlPanel); // 27
		omis.add(bar); // 28
		omis.add(table); // 29
		omis.add(tableswithDrawer); // 30
		omis.add(drawer); // 31
		omis.add(lockedDoor1); // 32
		omis.add(lockedDoor2); // 33
		omis.add(lockedDoor3); // 34
		omis.add(scarecrow); // 35
		omis.add(wallHolder); // 36
		omis.add(warningSign); // 37
		omis.add(boardComputer); //38
		omis.add(aliendead);//39
		omis.add(toolkitcase);//40
		omis.add(floor2);
		omis.add(floor3);
		omis.add(floor4);
	}
}
