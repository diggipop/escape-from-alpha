package EscapeFromAlpha;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class TitleScene extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JFrame window;
	JPanel startButtonPanel, mainTextPanel;
	JLabel titleNameLabel, timerLabel;
	JButton startButton;
	JTextArea maintextArea;
	JTextField inputtextField;
	Font titleFont = new Font("Times New Roman", Font.PLAIN, 50);
	Font normalFont = new Font("Times New Roman", Font.PLAIN, 20);
	TitleScreenHandler tsHandler = new TitleScreenHandler();
	
	private Image backgroundImage;

	public TitleScene(URL pathToImage) {
		try {
			backgroundImage = ImageIO.read(pathToImage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setLayout(null);
		
		
		titleNameLabel = new JLabel("ESCAPE FROM ALPHA");
		titleNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		titleNameLabel.setBounds(0, 0 , 0, 0);
		titleNameLabel.setForeground(Color.white);
		titleNameLabel.setFont(titleFont);
		titleNameLabel.setOpaque(false);
		titleNameLabel.setBackground(new Color(0,0,0,0));
		add(titleNameLabel);
		
		startButton = new JButton("START");
		startButton.setBounds(0, 0, 0, 0);
		startButton.setBackground(new Color(0,0,0,0));
		startButton.setForeground(Color.white);
		startButton.setFont(normalFont);
		startButton.addActionListener(tsHandler);
		startButton.setFocusable(false);
		add(startButton);
	}

	public void createGameScreen() {
		startButtonPanel.setVisible(false);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);
		
		titleNameLabel.setBounds(
				0,
				GameWindow.currentWindow.getBounds().height / 2 - titleNameLabel.getBounds().height - 50,
				GameWindow.currentWindow.getBounds().width,
				100);
		
		int width = GameWindow.currentWindow.getBounds().width / 5;
		startButton.setBounds(
				GameWindow.currentWindow.getBounds().width / 2 - width / 2,
				GameWindow.currentWindow.getBounds().height / 2 - titleNameLabel.getBounds().height + 50,
				width,
				40);		
		
		
		this.repaint();
	}
	
	public class TitleScreenHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			GameLogic.currentGame.startGame();
		}
	}

}