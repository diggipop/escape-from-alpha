package EscapeFromAlpha;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PanelObserveList extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Image backgroundImage;
	private DefaultListModel<CustomListEntry> data = new DefaultListModel<CustomListEntry>();
	private JList<CustomListEntry> list;

	public void update() {
		backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
		data.removeAllElements();

		for (Omi omi : GameLogic.currentGame.currentRoom.containedOmis) {
			data.addElement(new CustomListEntry(omi.name, false, omi.observed));
			if (omi.observed) {
				for (BaseItem item : omi.containedItems) {
					data.addElement(new CustomListEntry(item.name, false, item.observed));
				}
			}
		}

		for (BaseItem item : Inventar.myInv) {
			data.addElement(new CustomListEntry(item.name, true, item.observed));
		}
	}
	
	public PanelObserveList() {
		setBackground(Color.GREEN);
		setBorder(new EmptyBorder(50, 150, 50, 150));
		setLayout(new BorderLayout(0, 0));

		list = new JList<CustomListEntry>(data);
		list.setCellRenderer(new CustomCellRenderer());
		list.setFont(new Font("Arial", Font.PLAIN, 16));
		list.setForeground(Color.WHITE);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (list.getSelectedIndex() >= 0) {
					GameLogic.currentGame.observeListSelectionChanged(((CustomListEntry)list.getSelectedValue()).text);
				}
			}
		});
		list.setBackground(new Color(0, 0, 0, .5f));
		add(list, BorderLayout.CENTER);

		JTextPane header = new JTextPane();
		header.setForeground(Color.WHITE);
		header.setFont(new Font("Arial", Font.PLAIN, 22));
		header.setBackground(new Color(0, 0, 0, .5f));
		header.setText("Was m�chtest du dir genauer ansehen?");
		header.setEditable(false);
		add(header, BorderLayout.NORTH);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.paintComponents(g);
	}
}
