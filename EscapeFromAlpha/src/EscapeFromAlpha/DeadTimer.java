package EscapeFromAlpha;

import java.util.Timer;
import java.util.TimerTask;

public class DeadTimer {
	static int interval;
	static Timer timer;

	DeadTimer(String time) {
		String secs = time;
		int delay = 1000;
		int period = 1000;
		timer = new Timer();
		interval = Integer.parseInt(secs);
		timer.scheduleAtFixedRate(new TimerTask() {

			public void run() {
				setInterval();
				
				GameLogic.currentGame.timerChanged(interval);
				
				if(DeadTimer.interval <= 0)
					GameLogic.currentGame.timerEnded();
			}
		}, delay, period);
	}

	private static final int setInterval() {
		if (interval == 1)
			timer.cancel();
		return --interval;
	}
}
