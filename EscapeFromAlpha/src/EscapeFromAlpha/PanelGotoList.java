package EscapeFromAlpha;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class PanelGotoList extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Image backgroundImage;
	private DefaultListModel<String> data = new DefaultListModel<String>();
	private JList<String> list;

	public void update() {
		backgroundImage = GameLogic.currentGame.currentRoom.backgroundImage;
		data.removeAllElements();

		for(Room room : GameLogic.currentGame.currentRoom.connections) {
			System.out.println("Add " + room.name + "to list");
			data.addElement(room.name);
		}
	}
	/**
	 * Create the panel.
	 */
	public PanelGotoList() {
		setBackground(Color.GREEN);
		setBorder(new EmptyBorder(50, 150, 50, 150));
		setLayout(new BorderLayout(0, 0));
		
		list = new JList<String>(data);
		list.setFont(new Font("Arial", Font.PLAIN, 16));
		list.setForeground(Color.WHITE);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if(list.getSelectedIndex() >= 0) {
					GameLogic.currentGame.gotoListSelectionChanged((String)list.getSelectedValue());
				}
			}
		});
		list.setBackground(new Color(0,0,0,.5f));
		add(list, BorderLayout.CENTER);
		
		JTextPane header = new JTextPane();
		header.setForeground(Color.WHITE);
		header.setFont(new Font("Arial", Font.PLAIN, 22));
		header.setBackground(new Color(0,0,0,.5f));
		header.setText("Wohin m�chtest du gehen?");
		header.setEditable(false);
		add(header, BorderLayout.NORTH);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
	    g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(),null);
	    
	    this.paintComponents(g);
	}
}
