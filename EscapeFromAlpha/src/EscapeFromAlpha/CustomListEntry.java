package EscapeFromAlpha;

public class CustomListEntry {
	String text;
	Boolean isInInventory;
	Boolean isObserved;
	
	CustomListEntry(String text, Boolean isItem, Boolean isObserved) {
		this.text = text;
		this.isInInventory = isItem;
		this.isObserved = isObserved;
	}
	
	@Override
	public String toString() {
		return this.text;
	}
}
