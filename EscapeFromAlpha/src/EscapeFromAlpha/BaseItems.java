package EscapeFromAlpha;

import java.util.ArrayList;

public class BaseItems {
	static ArrayList<BaseItem> baseItems;
	
	BaseItems() {
		baseItems = new ArrayList<BaseItem>();
		
		for(BaseItem obs : Items.items) {
			baseItems.add(obs);
		}
		
		for(BaseItem obs : Omis.omis) {
			baseItems.add(obs);
		}
	}
	
	public static BaseItem getByName(String name) {
		System.out.println("Start search for " + name);
		for(BaseItem obs : baseItems) {
			if(obs.name.equals(name))
				return obs;
		}
		return null;
	}
}
