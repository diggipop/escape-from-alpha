package EscapeFromAlpha;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class EndScene extends JPanel implements MouseListener{
	
	
	
	private static final long serialVersionUID = 1L;
	/**
	    public static EndScene end1, end2, end3;
	    
	    
	    enum Scenes {
			Game, GameOver1, GameOver2, GameOver3
		};
	 
	 * 
		end1 = new EndScene ("Game Over A",30,"Hier steht drin warum du auf diese Art gestorben bist",12,Color.WHITE,Color.BLUE,"Arial");
		end2 = new EndScene ("Game Over B",30,"Hier steht drin warum du auf die andere Art gestorben bist",12,Color.WHITE,Color.RED,"Arial");
		end3 = new EndScene ("Game Over C",30,"Hier steht drin warum du gestorben bist",12,Color.BLACK,Color.WHITE,"Arial");
		
		
		switchScene
		
		case GameOver1:
			setContentPane(end1);
			break;
		case GameOver2:
			setContentPane(end2);
			break;
		case GameOver3:
			setContentPane(end3);
			break;
		
		
	 */
	
	private Image backgroundImage;
	
	public EndScene() {
		setBackground(new Color(0,0,0,0));
		setForeground(Color.BLACK);
		setBorder(new EmptyBorder(50, 50, 50, 50));
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		add(panel, BorderLayout.NORTH);

		JLabel labelHeader = new JLabel("Game Over");
		labelHeader.setForeground(Color.WHITE);
		panel.add(labelHeader);
		labelHeader.setFont(new Font("Arial", Font.BOLD, 26));

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.DARK_GRAY);
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));

		JTextPane labelText = new JTextPane();
		labelText.setEditable(false);
		labelText.setText("laaaaaaaaaannger Text End Scene 1");
		labelText.setForeground(Color.WHITE);
		labelText.setBackground(Color.DARK_GRAY);
		labelText.setFont(new Font("Arial", Font.BOLD, 22));
		panel_1.add(labelText, BorderLayout.CENTER);
		
		StyledDocument doc = labelText.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);	
		
		//labelText.setHorizontalAlignment(SwingConstants.CENTER);
		//panel_1.addMouseListener(this);
        addMouseListener(this);
        
        try {
			backgroundImage = ImageIO.read(new File("src\\ressources\\images\\EndDead.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
/**
 * 
 * @param _label Text1
 * @param _size1 Text1 Size
 * @param _text Text2
 * @param _size2 Text2 Size
 * @param _fontColor Textfarbe
 * @param _bgColor Hintergrundfarbe
 * @param _font Schriftart
 */
	
	public EndScene(String _label,int _size1,
			String _text,int _size2,
			Color _fontColor,Color _bgColor,
			String _font,
			String _pathToImage) {
		setBackground(new Color(0,0,0,0));
		setForeground(Color.BLACK);
		setBorder(new EmptyBorder(50, 50, 50, 50));
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBackground(_bgColor);
		add(panel, BorderLayout.NORTH);

		JLabel labelHeader = new JLabel(_label);
		labelHeader.setForeground(_fontColor);
		panel.add(labelHeader);
		labelHeader.setFont(new Font(_font, Font.BOLD, _size1));

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(_bgColor);
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));

		JTextPane labelText = new JTextPane();
		labelText.setText(_text);
		labelText.setForeground(_fontColor);
		labelText.setBackground(_bgColor);
		labelText.setFont(new Font(_font, Font.BOLD, _size2));
		panel_1.add(labelText, BorderLayout.CENTER);
		
		StyledDocument doc = labelText.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);	
		
		//labelText.setHorizontalAlignment(SwingConstants.CENTER);
		//panel_1.addMouseListener(this);
        addMouseListener(this);
        
        try {
			backgroundImage = ImageIO.read(GameWindow.class.getResource(_pathToImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);

		this.repaint();
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {
		GameWindow.currentWindow.switchScene(Scenes.Credits);
		removeMouseListener(this);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}



	@Override
	public void mouseClicked(MouseEvent e) {
	}



	@Override
	public void mouseEntered(MouseEvent e) {
	}



	@Override
	public void mouseExited(MouseEvent e) {
	}
}